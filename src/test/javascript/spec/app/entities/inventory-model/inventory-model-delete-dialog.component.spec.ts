/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { HipcarzTestModule } from '../../../test.module';
import { InventoryModelDeleteDialogComponent } from 'app/entities/inventory-model/inventory-model-delete-dialog.component';
import { InventoryModelService } from 'app/entities/inventory-model/inventory-model.service';

describe('Component Tests', () => {
  describe('InventoryModel Management Delete Component', () => {
    let comp: InventoryModelDeleteDialogComponent;
    let fixture: ComponentFixture<InventoryModelDeleteDialogComponent>;
    let service: InventoryModelService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HipcarzTestModule],
        declarations: [InventoryModelDeleteDialogComponent]
      })
        .overrideTemplate(InventoryModelDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(InventoryModelDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(InventoryModelService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
