/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { HipcarzTestModule } from '../../../test.module';
import { InventoryModelUpdateComponent } from 'app/entities/inventory-model/inventory-model-update.component';
import { InventoryModelService } from 'app/entities/inventory-model/inventory-model.service';
import { InventoryModel } from 'app/shared/model/inventory-model.model';

describe('Component Tests', () => {
  describe('InventoryModel Management Update Component', () => {
    let comp: InventoryModelUpdateComponent;
    let fixture: ComponentFixture<InventoryModelUpdateComponent>;
    let service: InventoryModelService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HipcarzTestModule],
        declarations: [InventoryModelUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(InventoryModelUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(InventoryModelUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(InventoryModelService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new InventoryModel(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new InventoryModel();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
