/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { HipcarzTestModule } from '../../../test.module';
import { InventoryModelDetailComponent } from 'app/entities/inventory-model/inventory-model-detail.component';
import { InventoryModel } from 'app/shared/model/inventory-model.model';

describe('Component Tests', () => {
  describe('InventoryModel Management Detail Component', () => {
    let comp: InventoryModelDetailComponent;
    let fixture: ComponentFixture<InventoryModelDetailComponent>;
    const route = ({ data: of({ inventoryModel: new InventoryModel(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HipcarzTestModule],
        declarations: [InventoryModelDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(InventoryModelDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(InventoryModelDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.inventoryModel).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
