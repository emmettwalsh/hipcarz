/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { HipcarzTestModule } from '../../../test.module';
import { MakeUpdateComponent } from 'app/entities/make/make-update.component';
import { MakeService } from 'app/entities/make/make.service';
import { Make } from 'app/shared/model/make.model';

describe('Component Tests', () => {
  describe('Make Management Update Component', () => {
    let comp: MakeUpdateComponent;
    let fixture: ComponentFixture<MakeUpdateComponent>;
    let service: MakeService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HipcarzTestModule],
        declarations: [MakeUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(MakeUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MakeUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MakeService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Make(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Make();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
