/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { HipcarzTestModule } from '../../../test.module';
import { MakeDeleteDialogComponent } from 'app/entities/make/make-delete-dialog.component';
import { MakeService } from 'app/entities/make/make.service';

describe('Component Tests', () => {
  describe('Make Management Delete Component', () => {
    let comp: MakeDeleteDialogComponent;
    let fixture: ComponentFixture<MakeDeleteDialogComponent>;
    let service: MakeService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HipcarzTestModule],
        declarations: [MakeDeleteDialogComponent]
      })
        .overrideTemplate(MakeDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MakeDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MakeService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
