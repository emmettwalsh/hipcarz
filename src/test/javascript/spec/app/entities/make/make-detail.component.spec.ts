/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { HipcarzTestModule } from '../../../test.module';
import { MakeDetailComponent } from 'app/entities/make/make-detail.component';
import { Make } from 'app/shared/model/make.model';

describe('Component Tests', () => {
  describe('Make Management Detail Component', () => {
    let comp: MakeDetailComponent;
    let fixture: ComponentFixture<MakeDetailComponent>;
    const route = ({ data: of({ make: new Make(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HipcarzTestModule],
        declarations: [MakeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(MakeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MakeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.make).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
