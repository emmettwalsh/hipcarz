import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class InventoryComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-inventory div table .btn-danger'));
  title = element.all(by.css('jhi-inventory div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class InventoryUpdatePage {
  pageTitle = element(by.id('jhi-inventory-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  vinInput = element(by.id('field_vin'));
  yearInput = element(by.id('field_year'));
  colorInput = element(by.id('field_color'));
  commentInput = element(by.id('field_comment'));
  inventoryModelSelect = element(by.id('field_inventoryModel'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setVinInput(vin) {
    await this.vinInput.sendKeys(vin);
  }

  async getVinInput() {
    return await this.vinInput.getAttribute('value');
  }

  async setYearInput(year) {
    await this.yearInput.sendKeys(year);
  }

  async getYearInput() {
    return await this.yearInput.getAttribute('value');
  }

  async setColorInput(color) {
    await this.colorInput.sendKeys(color);
  }

  async getColorInput() {
    return await this.colorInput.getAttribute('value');
  }

  async setCommentInput(comment) {
    await this.commentInput.sendKeys(comment);
  }

  async getCommentInput() {
    return await this.commentInput.getAttribute('value');
  }

  async inventoryModelSelectLastOption(timeout?: number) {
    await this.inventoryModelSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async inventoryModelSelectOption(option) {
    await this.inventoryModelSelect.sendKeys(option);
  }

  getInventoryModelSelect(): ElementFinder {
    return this.inventoryModelSelect;
  }

  async getInventoryModelSelectedOption() {
    return await this.inventoryModelSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class InventoryDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-inventory-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-inventory'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
