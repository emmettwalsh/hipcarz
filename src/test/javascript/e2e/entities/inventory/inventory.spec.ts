/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { InventoryComponentsPage, InventoryDeleteDialog, InventoryUpdatePage } from './inventory.page-object';

const expect = chai.expect;

describe('Inventory e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let inventoryUpdatePage: InventoryUpdatePage;
  let inventoryComponentsPage: InventoryComponentsPage;
  /*let inventoryDeleteDialog: InventoryDeleteDialog;*/

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Inventories', async () => {
    await navBarPage.goToEntity('inventory');
    inventoryComponentsPage = new InventoryComponentsPage();
    await browser.wait(ec.visibilityOf(inventoryComponentsPage.title), 5000);
    expect(await inventoryComponentsPage.getTitle()).to.eq('hipcarzApp.inventory.home.title');
  });

  it('should load create Inventory page', async () => {
    await inventoryComponentsPage.clickOnCreateButton();
    inventoryUpdatePage = new InventoryUpdatePage();
    expect(await inventoryUpdatePage.getPageTitle()).to.eq('hipcarzApp.inventory.home.createOrEditLabel');
    await inventoryUpdatePage.cancel();
  });

  /* it('should create and save Inventories', async () => {
        const nbButtonsBeforeCreate = await inventoryComponentsPage.countDeleteButtons();

        await inventoryComponentsPage.clickOnCreateButton();
        await promise.all([
            inventoryUpdatePage.setVinInput('vin'),
            inventoryUpdatePage.setYearInput('5'),
            inventoryUpdatePage.setColorInput('color'),
            inventoryUpdatePage.setCommentInput('comment'),
            inventoryUpdatePage.inventoryModelSelectLastOption(),
        ]);
        expect(await inventoryUpdatePage.getVinInput()).to.eq('vin', 'Expected Vin value to be equals to vin');
        expect(await inventoryUpdatePage.getYearInput()).to.eq('5', 'Expected year value to be equals to 5');
        expect(await inventoryUpdatePage.getColorInput()).to.eq('color', 'Expected Color value to be equals to color');
        expect(await inventoryUpdatePage.getCommentInput()).to.eq('comment', 'Expected Comment value to be equals to comment');
        await inventoryUpdatePage.save();
        expect(await inventoryUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await inventoryComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    });*/

  /* it('should delete last Inventory', async () => {
        const nbButtonsBeforeDelete = await inventoryComponentsPage.countDeleteButtons();
        await inventoryComponentsPage.clickOnLastDeleteButton();

        inventoryDeleteDialog = new InventoryDeleteDialog();
        expect(await inventoryDeleteDialog.getDialogTitle())
            .to.eq('hipcarzApp.inventory.delete.question');
        await inventoryDeleteDialog.clickOnConfirmButton();

        expect(await inventoryComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });*/

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
