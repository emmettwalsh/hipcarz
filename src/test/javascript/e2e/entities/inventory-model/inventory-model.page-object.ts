import { browser, ExpectedConditions, element, by, ElementFinder } from 'protractor';

export class InventoryModelComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-inventory-model div table .btn-danger'));
  title = element.all(by.css('jhi-inventory-model div h2#page-heading span')).first();

  async clickOnCreateButton(timeout?: number) {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(timeout?: number) {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons() {
    return this.deleteButtons.count();
  }

  async getTitle() {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class InventoryModelUpdatePage {
  pageTitle = element(by.id('jhi-inventory-model-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));
  nameInput = element(by.id('field_name'));
  vehicleTypeSelect = element(by.id('field_vehicleType'));
  descInput = element(by.id('field_desc'));
  makeSelect = element(by.id('field_make'));

  async getPageTitle() {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setNameInput(name) {
    await this.nameInput.sendKeys(name);
  }

  async getNameInput() {
    return await this.nameInput.getAttribute('value');
  }

  async setVehicleTypeSelect(vehicleType) {
    await this.vehicleTypeSelect.sendKeys(vehicleType);
  }

  async getVehicleTypeSelect() {
    return await this.vehicleTypeSelect.element(by.css('option:checked')).getText();
  }

  async vehicleTypeSelectLastOption(timeout?: number) {
    await this.vehicleTypeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async setDescInput(desc) {
    await this.descInput.sendKeys(desc);
  }

  async getDescInput() {
    return await this.descInput.getAttribute('value');
  }

  async makeSelectLastOption(timeout?: number) {
    await this.makeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  async makeSelectOption(option) {
    await this.makeSelect.sendKeys(option);
  }

  getMakeSelect(): ElementFinder {
    return this.makeSelect;
  }

  async getMakeSelectedOption() {
    return await this.makeSelect.element(by.css('option:checked')).getText();
  }

  async save(timeout?: number) {
    await this.saveButton.click();
  }

  async cancel(timeout?: number) {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class InventoryModelDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-inventoryModel-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-inventoryModel'));

  async getDialogTitle() {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(timeout?: number) {
    await this.confirmButton.click();
  }
}
