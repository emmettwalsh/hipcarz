/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { InventoryModelComponentsPage, InventoryModelDeleteDialog, InventoryModelUpdatePage } from './inventory-model.page-object';

const expect = chai.expect;

describe('InventoryModel e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let inventoryModelUpdatePage: InventoryModelUpdatePage;
  let inventoryModelComponentsPage: InventoryModelComponentsPage;
  /*let inventoryModelDeleteDialog: InventoryModelDeleteDialog;*/

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load InventoryModels', async () => {
    await navBarPage.goToEntity('inventory-model');
    inventoryModelComponentsPage = new InventoryModelComponentsPage();
    await browser.wait(ec.visibilityOf(inventoryModelComponentsPage.title), 5000);
    expect(await inventoryModelComponentsPage.getTitle()).to.eq('hipcarzApp.inventoryModel.home.title');
  });

  it('should load create InventoryModel page', async () => {
    await inventoryModelComponentsPage.clickOnCreateButton();
    inventoryModelUpdatePage = new InventoryModelUpdatePage();
    expect(await inventoryModelUpdatePage.getPageTitle()).to.eq('hipcarzApp.inventoryModel.home.createOrEditLabel');
    await inventoryModelUpdatePage.cancel();
  });

  /* it('should create and save InventoryModels', async () => {
        const nbButtonsBeforeCreate = await inventoryModelComponentsPage.countDeleteButtons();

        await inventoryModelComponentsPage.clickOnCreateButton();
        await promise.all([
            inventoryModelUpdatePage.setNameInput('name'),
            inventoryModelUpdatePage.vehicleTypeSelectLastOption(),
            inventoryModelUpdatePage.setDescInput('desc'),
            inventoryModelUpdatePage.makeSelectLastOption(),
        ]);
        expect(await inventoryModelUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
        expect(await inventoryModelUpdatePage.getDescInput()).to.eq('desc', 'Expected Desc value to be equals to desc');
        await inventoryModelUpdatePage.save();
        expect(await inventoryModelUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

        expect(await inventoryModelComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
    });*/

  /* it('should delete last InventoryModel', async () => {
        const nbButtonsBeforeDelete = await inventoryModelComponentsPage.countDeleteButtons();
        await inventoryModelComponentsPage.clickOnLastDeleteButton();

        inventoryModelDeleteDialog = new InventoryModelDeleteDialog();
        expect(await inventoryModelDeleteDialog.getDialogTitle())
            .to.eq('hipcarzApp.inventoryModel.delete.question');
        await inventoryModelDeleteDialog.clickOnConfirmButton();

        expect(await inventoryModelComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });*/

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
