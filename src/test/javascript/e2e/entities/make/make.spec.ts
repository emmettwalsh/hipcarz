/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { MakeComponentsPage, MakeDeleteDialog, MakeUpdatePage } from './make.page-object';

const expect = chai.expect;

describe('Make e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let makeUpdatePage: MakeUpdatePage;
  let makeComponentsPage: MakeComponentsPage;
  let makeDeleteDialog: MakeDeleteDialog;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.autoSignInUsing('admin', 'admin');
    await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
  });

  it('should load Makes', async () => {
    await navBarPage.goToEntity('make');
    makeComponentsPage = new MakeComponentsPage();
    await browser.wait(ec.visibilityOf(makeComponentsPage.title), 5000);
    expect(await makeComponentsPage.getTitle()).to.eq('hipcarzApp.make.home.title');
  });

  it('should load create Make page', async () => {
    await makeComponentsPage.clickOnCreateButton();
    makeUpdatePage = new MakeUpdatePage();
    expect(await makeUpdatePage.getPageTitle()).to.eq('hipcarzApp.make.home.createOrEditLabel');
    await makeUpdatePage.cancel();
  });

  it('should create and save Makes', async () => {
    const nbButtonsBeforeCreate = await makeComponentsPage.countDeleteButtons();

    await makeComponentsPage.clickOnCreateButton();
    await promise.all([makeUpdatePage.setNameInput('name'), makeUpdatePage.setDescInput('desc')]);
    expect(await makeUpdatePage.getNameInput()).to.eq('name', 'Expected Name value to be equals to name');
    expect(await makeUpdatePage.getDescInput()).to.eq('desc', 'Expected Desc value to be equals to desc');
    await makeUpdatePage.save();
    expect(await makeUpdatePage.getSaveButton().isPresent(), 'Expected save button disappear').to.be.false;

    expect(await makeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1, 'Expected one more entry in the table');
  });

  it('should delete last Make', async () => {
    const nbButtonsBeforeDelete = await makeComponentsPage.countDeleteButtons();
    await makeComponentsPage.clickOnLastDeleteButton();

    makeDeleteDialog = new MakeDeleteDialog();
    expect(await makeDeleteDialog.getDialogTitle()).to.eq('hipcarzApp.make.delete.question');
    await makeDeleteDialog.clickOnConfirmButton();

    expect(await makeComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
