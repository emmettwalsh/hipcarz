package com.hipcarz.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link MakeSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class MakeSearchRepositoryMockConfiguration {

    @MockBean
    private MakeSearchRepository mockMakeSearchRepository;

}
