package com.hipcarz.repository.search;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

/**
 * Configure a Mock version of {@link InventoryModelSearchRepository} to test the
 * application without starting Elasticsearch.
 */
@Configuration
public class InventoryModelSearchRepositoryMockConfiguration {

    @MockBean
    private InventoryModelSearchRepository mockInventoryModelSearchRepository;

}
