package com.hipcarz.web.rest;

import com.hipcarz.HipcarzApp;
import com.hipcarz.domain.InventoryModel;
import com.hipcarz.domain.Make;
import com.hipcarz.repository.InventoryModelRepository;
import com.hipcarz.repository.search.InventoryModelSearchRepository;
import com.hipcarz.service.InventoryModelService;
import com.hipcarz.service.dto.InventoryModelDTO;
import com.hipcarz.service.mapper.InventoryModelMapper;
import com.hipcarz.web.rest.errors.ExceptionTranslator;
import com.hipcarz.service.dto.InventoryModelCriteria;
import com.hipcarz.service.InventoryModelQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static com.hipcarz.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.hipcarz.domain.enumeration.VehicleType;
/**
 * Integration tests for the {@Link InventoryModelResource} REST controller.
 */
@SpringBootTest(classes = HipcarzApp.class)
public class InventoryModelResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final VehicleType DEFAULT_VEHICLE_TYPE = VehicleType.SUV;
    private static final VehicleType UPDATED_VEHICLE_TYPE = VehicleType.SEDAN;

    private static final String DEFAULT_DESC = "AAAAAAAAAA";
    private static final String UPDATED_DESC = "BBBBBBBBBB";

    @Autowired
    private InventoryModelRepository inventoryModelRepository;

    @Autowired
    private InventoryModelMapper inventoryModelMapper;

    @Autowired
    private InventoryModelService inventoryModelService;

    /**
     * This repository is mocked in the com.hipcarz.repository.search test package.
     *
     * @see com.hipcarz.repository.search.InventoryModelSearchRepositoryMockConfiguration
     */
    @Autowired
    private InventoryModelSearchRepository mockInventoryModelSearchRepository;

    @Autowired
    private InventoryModelQueryService inventoryModelQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restInventoryModelMockMvc;

    private InventoryModel inventoryModel;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final InventoryModelResource inventoryModelResource = new InventoryModelResource(inventoryModelService, inventoryModelQueryService);
        this.restInventoryModelMockMvc = MockMvcBuilders.standaloneSetup(inventoryModelResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InventoryModel createEntity(EntityManager em) {
        InventoryModel inventoryModel = new InventoryModel()
            .name(DEFAULT_NAME)
            .vehicleType(DEFAULT_VEHICLE_TYPE)
            .desc(DEFAULT_DESC);
        // Add required entity
        Make make;
        if (TestUtil.findAll(em, Make.class).isEmpty()) {
            make = MakeResourceIT.createEntity(em);
            em.persist(make);
            em.flush();
        } else {
            make = TestUtil.findAll(em, Make.class).get(0);
        }
        inventoryModel.setMake(make);
        return inventoryModel;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InventoryModel createUpdatedEntity(EntityManager em) {
        InventoryModel inventoryModel = new InventoryModel()
            .name(UPDATED_NAME)
            .vehicleType(UPDATED_VEHICLE_TYPE)
            .desc(UPDATED_DESC);
        // Add required entity
        Make make;
        if (TestUtil.findAll(em, Make.class).isEmpty()) {
            make = MakeResourceIT.createUpdatedEntity(em);
            em.persist(make);
            em.flush();
        } else {
            make = TestUtil.findAll(em, Make.class).get(0);
        }
        inventoryModel.setMake(make);
        return inventoryModel;
    }

    @BeforeEach
    public void initTest() {
        inventoryModel = createEntity(em);
    }

    @Test
    @Transactional
    public void createInventoryModel() throws Exception {
        int databaseSizeBeforeCreate = inventoryModelRepository.findAll().size();

        // Create the InventoryModel
        InventoryModelDTO inventoryModelDTO = inventoryModelMapper.toDto(inventoryModel);
        restInventoryModelMockMvc.perform(post("/api/inventory-models")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inventoryModelDTO)))
            .andExpect(status().isCreated());

        // Validate the InventoryModel in the database
        List<InventoryModel> inventoryModelList = inventoryModelRepository.findAll();
        assertThat(inventoryModelList).hasSize(databaseSizeBeforeCreate + 1);
        InventoryModel testInventoryModel = inventoryModelList.get(inventoryModelList.size() - 1);
        assertThat(testInventoryModel.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testInventoryModel.getVehicleType()).isEqualTo(DEFAULT_VEHICLE_TYPE);
        assertThat(testInventoryModel.getDesc()).isEqualTo(DEFAULT_DESC);

        // Validate the InventoryModel in Elasticsearch
        verify(mockInventoryModelSearchRepository, times(1)).save(testInventoryModel);
    }

    @Test
    @Transactional
    public void createInventoryModelWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = inventoryModelRepository.findAll().size();

        // Create the InventoryModel with an existing ID
        inventoryModel.setId(1L);
        InventoryModelDTO inventoryModelDTO = inventoryModelMapper.toDto(inventoryModel);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInventoryModelMockMvc.perform(post("/api/inventory-models")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inventoryModelDTO)))
            .andExpect(status().isBadRequest());

        // Validate the InventoryModel in the database
        List<InventoryModel> inventoryModelList = inventoryModelRepository.findAll();
        assertThat(inventoryModelList).hasSize(databaseSizeBeforeCreate);

        // Validate the InventoryModel in Elasticsearch
        verify(mockInventoryModelSearchRepository, times(0)).save(inventoryModel);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = inventoryModelRepository.findAll().size();
        // set the field null
        inventoryModel.setName(null);

        // Create the InventoryModel, which fails.
        InventoryModelDTO inventoryModelDTO = inventoryModelMapper.toDto(inventoryModel);

        restInventoryModelMockMvc.perform(post("/api/inventory-models")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inventoryModelDTO)))
            .andExpect(status().isBadRequest());

        List<InventoryModel> inventoryModelList = inventoryModelRepository.findAll();
        assertThat(inventoryModelList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllInventoryModels() throws Exception {
        // Initialize the database
        inventoryModelRepository.saveAndFlush(inventoryModel);

        // Get all the inventoryModelList
        restInventoryModelMockMvc.perform(get("/api/inventory-models?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(inventoryModel.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].vehicleType").value(hasItem(DEFAULT_VEHICLE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].desc").value(hasItem(DEFAULT_DESC.toString())));
    }
    
    @Test
    @Transactional
    public void getInventoryModel() throws Exception {
        // Initialize the database
        inventoryModelRepository.saveAndFlush(inventoryModel);

        // Get the inventoryModel
        restInventoryModelMockMvc.perform(get("/api/inventory-models/{id}", inventoryModel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(inventoryModel.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.vehicleType").value(DEFAULT_VEHICLE_TYPE.toString()))
            .andExpect(jsonPath("$.desc").value(DEFAULT_DESC.toString()));
    }

    @Test
    @Transactional
    public void getAllInventoryModelsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryModelRepository.saveAndFlush(inventoryModel);

        // Get all the inventoryModelList where name equals to DEFAULT_NAME
        defaultInventoryModelShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the inventoryModelList where name equals to UPDATED_NAME
        defaultInventoryModelShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllInventoryModelsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryModelRepository.saveAndFlush(inventoryModel);

        // Get all the inventoryModelList where name in DEFAULT_NAME or UPDATED_NAME
        defaultInventoryModelShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the inventoryModelList where name equals to UPDATED_NAME
        defaultInventoryModelShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllInventoryModelsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryModelRepository.saveAndFlush(inventoryModel);

        // Get all the inventoryModelList where name is not null
        defaultInventoryModelShouldBeFound("name.specified=true");

        // Get all the inventoryModelList where name is null
        defaultInventoryModelShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoryModelsByVehicleTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryModelRepository.saveAndFlush(inventoryModel);

        // Get all the inventoryModelList where vehicleType equals to DEFAULT_VEHICLE_TYPE
        defaultInventoryModelShouldBeFound("vehicleType.equals=" + DEFAULT_VEHICLE_TYPE);

        // Get all the inventoryModelList where vehicleType equals to UPDATED_VEHICLE_TYPE
        defaultInventoryModelShouldNotBeFound("vehicleType.equals=" + UPDATED_VEHICLE_TYPE);
    }

    @Test
    @Transactional
    public void getAllInventoryModelsByVehicleTypeIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryModelRepository.saveAndFlush(inventoryModel);

        // Get all the inventoryModelList where vehicleType in DEFAULT_VEHICLE_TYPE or UPDATED_VEHICLE_TYPE
        defaultInventoryModelShouldBeFound("vehicleType.in=" + DEFAULT_VEHICLE_TYPE + "," + UPDATED_VEHICLE_TYPE);

        // Get all the inventoryModelList where vehicleType equals to UPDATED_VEHICLE_TYPE
        defaultInventoryModelShouldNotBeFound("vehicleType.in=" + UPDATED_VEHICLE_TYPE);
    }

    @Test
    @Transactional
    public void getAllInventoryModelsByVehicleTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryModelRepository.saveAndFlush(inventoryModel);

        // Get all the inventoryModelList where vehicleType is not null
        defaultInventoryModelShouldBeFound("vehicleType.specified=true");

        // Get all the inventoryModelList where vehicleType is null
        defaultInventoryModelShouldNotBeFound("vehicleType.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoryModelsByDescIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryModelRepository.saveAndFlush(inventoryModel);

        // Get all the inventoryModelList where desc equals to DEFAULT_DESC
        defaultInventoryModelShouldBeFound("desc.equals=" + DEFAULT_DESC);

        // Get all the inventoryModelList where desc equals to UPDATED_DESC
        defaultInventoryModelShouldNotBeFound("desc.equals=" + UPDATED_DESC);
    }

    @Test
    @Transactional
    public void getAllInventoryModelsByDescIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryModelRepository.saveAndFlush(inventoryModel);

        // Get all the inventoryModelList where desc in DEFAULT_DESC or UPDATED_DESC
        defaultInventoryModelShouldBeFound("desc.in=" + DEFAULT_DESC + "," + UPDATED_DESC);

        // Get all the inventoryModelList where desc equals to UPDATED_DESC
        defaultInventoryModelShouldNotBeFound("desc.in=" + UPDATED_DESC);
    }

    @Test
    @Transactional
    public void getAllInventoryModelsByDescIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryModelRepository.saveAndFlush(inventoryModel);

        // Get all the inventoryModelList where desc is not null
        defaultInventoryModelShouldBeFound("desc.specified=true");

        // Get all the inventoryModelList where desc is null
        defaultInventoryModelShouldNotBeFound("desc.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoryModelsByMakeIsEqualToSomething() throws Exception {
        // Get already existing entity
        Make make = inventoryModel.getMake();
        inventoryModelRepository.saveAndFlush(inventoryModel);
        Long makeId = make.getId();

        // Get all the inventoryModelList where make equals to makeId
        defaultInventoryModelShouldBeFound("makeId.equals=" + makeId);

        // Get all the inventoryModelList where make equals to makeId + 1
        defaultInventoryModelShouldNotBeFound("makeId.equals=" + (makeId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultInventoryModelShouldBeFound(String filter) throws Exception {
        restInventoryModelMockMvc.perform(get("/api/inventory-models?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(inventoryModel.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].vehicleType").value(hasItem(DEFAULT_VEHICLE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].desc").value(hasItem(DEFAULT_DESC)));

        // Check, that the count call also returns 1
        restInventoryModelMockMvc.perform(get("/api/inventory-models/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultInventoryModelShouldNotBeFound(String filter) throws Exception {
        restInventoryModelMockMvc.perform(get("/api/inventory-models?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restInventoryModelMockMvc.perform(get("/api/inventory-models/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingInventoryModel() throws Exception {
        // Get the inventoryModel
        restInventoryModelMockMvc.perform(get("/api/inventory-models/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInventoryModel() throws Exception {
        // Initialize the database
        inventoryModelRepository.saveAndFlush(inventoryModel);

        int databaseSizeBeforeUpdate = inventoryModelRepository.findAll().size();

        // Update the inventoryModel
        InventoryModel updatedInventoryModel = inventoryModelRepository.findById(inventoryModel.getId()).get();
        // Disconnect from session so that the updates on updatedInventoryModel are not directly saved in db
        em.detach(updatedInventoryModel);
        updatedInventoryModel
            .name(UPDATED_NAME)
            .vehicleType(UPDATED_VEHICLE_TYPE)
            .desc(UPDATED_DESC);
        InventoryModelDTO inventoryModelDTO = inventoryModelMapper.toDto(updatedInventoryModel);

        restInventoryModelMockMvc.perform(put("/api/inventory-models")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inventoryModelDTO)))
            .andExpect(status().isOk());

        // Validate the InventoryModel in the database
        List<InventoryModel> inventoryModelList = inventoryModelRepository.findAll();
        assertThat(inventoryModelList).hasSize(databaseSizeBeforeUpdate);
        InventoryModel testInventoryModel = inventoryModelList.get(inventoryModelList.size() - 1);
        assertThat(testInventoryModel.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testInventoryModel.getVehicleType()).isEqualTo(UPDATED_VEHICLE_TYPE);
        assertThat(testInventoryModel.getDesc()).isEqualTo(UPDATED_DESC);

        // Validate the InventoryModel in Elasticsearch
        verify(mockInventoryModelSearchRepository, times(1)).save(testInventoryModel);
    }

    @Test
    @Transactional
    public void updateNonExistingInventoryModel() throws Exception {
        int databaseSizeBeforeUpdate = inventoryModelRepository.findAll().size();

        // Create the InventoryModel
        InventoryModelDTO inventoryModelDTO = inventoryModelMapper.toDto(inventoryModel);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInventoryModelMockMvc.perform(put("/api/inventory-models")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inventoryModelDTO)))
            .andExpect(status().isBadRequest());

        // Validate the InventoryModel in the database
        List<InventoryModel> inventoryModelList = inventoryModelRepository.findAll();
        assertThat(inventoryModelList).hasSize(databaseSizeBeforeUpdate);

        // Validate the InventoryModel in Elasticsearch
        verify(mockInventoryModelSearchRepository, times(0)).save(inventoryModel);
    }

    @Test
    @Transactional
    public void deleteInventoryModel() throws Exception {
        // Initialize the database
        inventoryModelRepository.saveAndFlush(inventoryModel);

        int databaseSizeBeforeDelete = inventoryModelRepository.findAll().size();

        // Delete the inventoryModel
        restInventoryModelMockMvc.perform(delete("/api/inventory-models/{id}", inventoryModel.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<InventoryModel> inventoryModelList = inventoryModelRepository.findAll();
        assertThat(inventoryModelList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the InventoryModel in Elasticsearch
        verify(mockInventoryModelSearchRepository, times(1)).deleteById(inventoryModel.getId());
    }

    @Test
    @Transactional
    public void searchInventoryModel() throws Exception {
        // Initialize the database
        inventoryModelRepository.saveAndFlush(inventoryModel);
        when(mockInventoryModelSearchRepository.search(queryStringQuery("id:" + inventoryModel.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(inventoryModel), PageRequest.of(0, 1), 1));
        // Search the inventoryModel
        restInventoryModelMockMvc.perform(get("/api/_search/inventory-models?query=id:" + inventoryModel.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(inventoryModel.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].vehicleType").value(hasItem(DEFAULT_VEHICLE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].desc").value(hasItem(DEFAULT_DESC)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(InventoryModel.class);
        InventoryModel inventoryModel1 = new InventoryModel();
        inventoryModel1.setId(1L);
        InventoryModel inventoryModel2 = new InventoryModel();
        inventoryModel2.setId(inventoryModel1.getId());
        assertThat(inventoryModel1).isEqualTo(inventoryModel2);
        inventoryModel2.setId(2L);
        assertThat(inventoryModel1).isNotEqualTo(inventoryModel2);
        inventoryModel1.setId(null);
        assertThat(inventoryModel1).isNotEqualTo(inventoryModel2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(InventoryModelDTO.class);
        InventoryModelDTO inventoryModelDTO1 = new InventoryModelDTO();
        inventoryModelDTO1.setId(1L);
        InventoryModelDTO inventoryModelDTO2 = new InventoryModelDTO();
        assertThat(inventoryModelDTO1).isNotEqualTo(inventoryModelDTO2);
        inventoryModelDTO2.setId(inventoryModelDTO1.getId());
        assertThat(inventoryModelDTO1).isEqualTo(inventoryModelDTO2);
        inventoryModelDTO2.setId(2L);
        assertThat(inventoryModelDTO1).isNotEqualTo(inventoryModelDTO2);
        inventoryModelDTO1.setId(null);
        assertThat(inventoryModelDTO1).isNotEqualTo(inventoryModelDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(inventoryModelMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(inventoryModelMapper.fromId(null)).isNull();
    }
}
