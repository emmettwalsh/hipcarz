package com.hipcarz.web.rest;

import com.hipcarz.HipcarzApp;
import com.hipcarz.domain.Inventory;
import com.hipcarz.domain.InventoryModel;
import com.hipcarz.repository.InventoryRepository;
import com.hipcarz.repository.search.InventorySearchRepository;
import com.hipcarz.service.InventoryService;
import com.hipcarz.service.dto.InventoryDTO;
import com.hipcarz.service.mapper.InventoryMapper;
import com.hipcarz.web.rest.errors.ExceptionTranslator;
import com.hipcarz.service.dto.InventoryCriteria;
import com.hipcarz.service.InventoryQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static com.hipcarz.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link InventoryResource} REST controller.
 */
@SpringBootTest(classes = HipcarzApp.class)
public class InventoryResourceIT {

    private static final String DEFAULT_VIN = "AAAAAAAAAA";
    private static final String UPDATED_VIN = "BBBBBBBBBB";

    private static final Integer DEFAULT_YEAR = 1900;
    private static final Integer UPDATED_YEAR = 1901;

    private static final String DEFAULT_COLOR = "AAAAAAAAAA";
    private static final String UPDATED_COLOR = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENT = "BBBBBBBBBB";

    @Autowired
    private InventoryRepository inventoryRepository;

    @Autowired
    private InventoryMapper inventoryMapper;

    @Autowired
    private InventoryService inventoryService;

    /**
     * This repository is mocked in the com.hipcarz.repository.search test package.
     *
     * @see com.hipcarz.repository.search.InventorySearchRepositoryMockConfiguration
     */
    @Autowired
    private InventorySearchRepository mockInventorySearchRepository;

    @Autowired
    private InventoryQueryService inventoryQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restInventoryMockMvc;

    private Inventory inventory;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final InventoryResource inventoryResource = new InventoryResource(inventoryService, inventoryQueryService);
        this.restInventoryMockMvc = MockMvcBuilders.standaloneSetup(inventoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Inventory createEntity(EntityManager em) {
        Inventory inventory = new Inventory()
            .vin(DEFAULT_VIN)
            .year(DEFAULT_YEAR)
            .color(DEFAULT_COLOR)
            .comment(DEFAULT_COMMENT);
        // Add required entity
        InventoryModel inventoryModel;
        if (TestUtil.findAll(em, InventoryModel.class).isEmpty()) {
            inventoryModel = InventoryModelResourceIT.createEntity(em);
            em.persist(inventoryModel);
            em.flush();
        } else {
            inventoryModel = TestUtil.findAll(em, InventoryModel.class).get(0);
        }
        inventory.setInventoryModel(inventoryModel);
        return inventory;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Inventory createUpdatedEntity(EntityManager em) {
        Inventory inventory = new Inventory()
            .vin(UPDATED_VIN)
            .year(UPDATED_YEAR)
            .color(UPDATED_COLOR)
            .comment(UPDATED_COMMENT);
        // Add required entity
        InventoryModel inventoryModel;
        if (TestUtil.findAll(em, InventoryModel.class).isEmpty()) {
            inventoryModel = InventoryModelResourceIT.createUpdatedEntity(em);
            em.persist(inventoryModel);
            em.flush();
        } else {
            inventoryModel = TestUtil.findAll(em, InventoryModel.class).get(0);
        }
        inventory.setInventoryModel(inventoryModel);
        return inventory;
    }

    @BeforeEach
    public void initTest() {
        inventory = createEntity(em);
    }

    @Test
    @Transactional
    public void createInventory() throws Exception {
        int databaseSizeBeforeCreate = inventoryRepository.findAll().size();

        // Create the Inventory
        InventoryDTO inventoryDTO = inventoryMapper.toDto(inventory);
        restInventoryMockMvc.perform(post("/api/inventories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inventoryDTO)))
            .andExpect(status().isCreated());

        // Validate the Inventory in the database
        List<Inventory> inventoryList = inventoryRepository.findAll();
        assertThat(inventoryList).hasSize(databaseSizeBeforeCreate + 1);
        Inventory testInventory = inventoryList.get(inventoryList.size() - 1);
        assertThat(testInventory.getVin()).isEqualTo(DEFAULT_VIN);
        assertThat(testInventory.getYear()).isEqualTo(DEFAULT_YEAR);
        assertThat(testInventory.getColor()).isEqualTo(DEFAULT_COLOR);
        assertThat(testInventory.getComment()).isEqualTo(DEFAULT_COMMENT);

        // Validate the Inventory in Elasticsearch
        verify(mockInventorySearchRepository, times(1)).save(testInventory);
    }

    @Test
    @Transactional
    public void createInventoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = inventoryRepository.findAll().size();

        // Create the Inventory with an existing ID
        inventory.setId(1L);
        InventoryDTO inventoryDTO = inventoryMapper.toDto(inventory);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInventoryMockMvc.perform(post("/api/inventories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inventoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Inventory in the database
        List<Inventory> inventoryList = inventoryRepository.findAll();
        assertThat(inventoryList).hasSize(databaseSizeBeforeCreate);

        // Validate the Inventory in Elasticsearch
        verify(mockInventorySearchRepository, times(0)).save(inventory);
    }


    @Test
    @Transactional
    public void checkVinIsRequired() throws Exception {
        int databaseSizeBeforeTest = inventoryRepository.findAll().size();
        // set the field null
        inventory.setVin(null);

        // Create the Inventory, which fails.
        InventoryDTO inventoryDTO = inventoryMapper.toDto(inventory);

        restInventoryMockMvc.perform(post("/api/inventories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inventoryDTO)))
            .andExpect(status().isBadRequest());

        List<Inventory> inventoryList = inventoryRepository.findAll();
        assertThat(inventoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllInventories() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList
        restInventoryMockMvc.perform(get("/api/inventories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(inventory.getId().intValue())))
            .andExpect(jsonPath("$.[*].vin").value(hasItem(DEFAULT_VIN.toString())))
            .andExpect(jsonPath("$.[*].year").value(hasItem(DEFAULT_YEAR)))
            .andExpect(jsonPath("$.[*].color").value(hasItem(DEFAULT_COLOR.toString())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())));
    }
    
    @Test
    @Transactional
    public void getInventory() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get the inventory
        restInventoryMockMvc.perform(get("/api/inventories/{id}", inventory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(inventory.getId().intValue()))
            .andExpect(jsonPath("$.vin").value(DEFAULT_VIN.toString()))
            .andExpect(jsonPath("$.year").value(DEFAULT_YEAR))
            .andExpect(jsonPath("$.color").value(DEFAULT_COLOR.toString()))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT.toString()));
    }

    @Test
    @Transactional
    public void getAllInventoriesByVinIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where vin equals to DEFAULT_VIN
        defaultInventoryShouldBeFound("vin.equals=" + DEFAULT_VIN);

        // Get all the inventoryList where vin equals to UPDATED_VIN
        defaultInventoryShouldNotBeFound("vin.equals=" + UPDATED_VIN);
    }

    @Test
    @Transactional
    public void getAllInventoriesByVinIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where vin in DEFAULT_VIN or UPDATED_VIN
        defaultInventoryShouldBeFound("vin.in=" + DEFAULT_VIN + "," + UPDATED_VIN);

        // Get all the inventoryList where vin equals to UPDATED_VIN
        defaultInventoryShouldNotBeFound("vin.in=" + UPDATED_VIN);
    }

    @Test
    @Transactional
    public void getAllInventoriesByVinIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where vin is not null
        defaultInventoryShouldBeFound("vin.specified=true");

        // Get all the inventoryList where vin is null
        defaultInventoryShouldNotBeFound("vin.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoriesByYearIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where year equals to DEFAULT_YEAR
        defaultInventoryShouldBeFound("year.equals=" + DEFAULT_YEAR);

        // Get all the inventoryList where year equals to UPDATED_YEAR
        defaultInventoryShouldNotBeFound("year.equals=" + UPDATED_YEAR);
    }

    @Test
    @Transactional
    public void getAllInventoriesByYearIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where year in DEFAULT_YEAR or UPDATED_YEAR
        defaultInventoryShouldBeFound("year.in=" + DEFAULT_YEAR + "," + UPDATED_YEAR);

        // Get all the inventoryList where year equals to UPDATED_YEAR
        defaultInventoryShouldNotBeFound("year.in=" + UPDATED_YEAR);
    }

    @Test
    @Transactional
    public void getAllInventoriesByYearIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where year is not null
        defaultInventoryShouldBeFound("year.specified=true");

        // Get all the inventoryList where year is null
        defaultInventoryShouldNotBeFound("year.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoriesByYearIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where year greater than or equals to DEFAULT_YEAR
        defaultInventoryShouldBeFound("year.greaterOrEqualThan=" + DEFAULT_YEAR);

        // Get all the inventoryList where year greater than or equals to (DEFAULT_YEAR + 1)
        defaultInventoryShouldNotBeFound("year.greaterOrEqualThan=" + (DEFAULT_YEAR + 1));
    }

    @Test
    @Transactional
    public void getAllInventoriesByYearIsLessThanSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where year less than or equals to DEFAULT_YEAR
        defaultInventoryShouldNotBeFound("year.lessThan=" + DEFAULT_YEAR);

        // Get all the inventoryList where year less than or equals to (DEFAULT_YEAR + 1)
        defaultInventoryShouldBeFound("year.lessThan=" + (DEFAULT_YEAR + 1));
    }


    @Test
    @Transactional
    public void getAllInventoriesByColorIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where color equals to DEFAULT_COLOR
        defaultInventoryShouldBeFound("color.equals=" + DEFAULT_COLOR);

        // Get all the inventoryList where color equals to UPDATED_COLOR
        defaultInventoryShouldNotBeFound("color.equals=" + UPDATED_COLOR);
    }

    @Test
    @Transactional
    public void getAllInventoriesByColorIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where color in DEFAULT_COLOR or UPDATED_COLOR
        defaultInventoryShouldBeFound("color.in=" + DEFAULT_COLOR + "," + UPDATED_COLOR);

        // Get all the inventoryList where color equals to UPDATED_COLOR
        defaultInventoryShouldNotBeFound("color.in=" + UPDATED_COLOR);
    }

    @Test
    @Transactional
    public void getAllInventoriesByColorIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where color is not null
        defaultInventoryShouldBeFound("color.specified=true");

        // Get all the inventoryList where color is null
        defaultInventoryShouldNotBeFound("color.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoriesByCommentIsEqualToSomething() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where comment equals to DEFAULT_COMMENT
        defaultInventoryShouldBeFound("comment.equals=" + DEFAULT_COMMENT);

        // Get all the inventoryList where comment equals to UPDATED_COMMENT
        defaultInventoryShouldNotBeFound("comment.equals=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    public void getAllInventoriesByCommentIsInShouldWork() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where comment in DEFAULT_COMMENT or UPDATED_COMMENT
        defaultInventoryShouldBeFound("comment.in=" + DEFAULT_COMMENT + "," + UPDATED_COMMENT);

        // Get all the inventoryList where comment equals to UPDATED_COMMENT
        defaultInventoryShouldNotBeFound("comment.in=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    public void getAllInventoriesByCommentIsNullOrNotNull() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        // Get all the inventoryList where comment is not null
        defaultInventoryShouldBeFound("comment.specified=true");

        // Get all the inventoryList where comment is null
        defaultInventoryShouldNotBeFound("comment.specified=false");
    }

    @Test
    @Transactional
    public void getAllInventoriesByInventoryModelIsEqualToSomething() throws Exception {
        // Get already existing entity
        InventoryModel inventoryModel = inventory.getInventoryModel();
        inventoryRepository.saveAndFlush(inventory);
        Long inventoryModelId = inventoryModel.getId();

        // Get all the inventoryList where inventoryModel equals to inventoryModelId
        defaultInventoryShouldBeFound("inventoryModelId.equals=" + inventoryModelId);

        // Get all the inventoryList where inventoryModel equals to inventoryModelId + 1
        defaultInventoryShouldNotBeFound("inventoryModelId.equals=" + (inventoryModelId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultInventoryShouldBeFound(String filter) throws Exception {
        restInventoryMockMvc.perform(get("/api/inventories?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(inventory.getId().intValue())))
            .andExpect(jsonPath("$.[*].vin").value(hasItem(DEFAULT_VIN)))
            .andExpect(jsonPath("$.[*].year").value(hasItem(DEFAULT_YEAR)))
            .andExpect(jsonPath("$.[*].color").value(hasItem(DEFAULT_COLOR)))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT)));

        // Check, that the count call also returns 1
        restInventoryMockMvc.perform(get("/api/inventories/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultInventoryShouldNotBeFound(String filter) throws Exception {
        restInventoryMockMvc.perform(get("/api/inventories?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restInventoryMockMvc.perform(get("/api/inventories/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingInventory() throws Exception {
        // Get the inventory
        restInventoryMockMvc.perform(get("/api/inventories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInventory() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        int databaseSizeBeforeUpdate = inventoryRepository.findAll().size();

        // Update the inventory
        Inventory updatedInventory = inventoryRepository.findById(inventory.getId()).get();
        // Disconnect from session so that the updates on updatedInventory are not directly saved in db
        em.detach(updatedInventory);
        updatedInventory
            .vin(UPDATED_VIN)
            .year(UPDATED_YEAR)
            .color(UPDATED_COLOR)
            .comment(UPDATED_COMMENT);
        InventoryDTO inventoryDTO = inventoryMapper.toDto(updatedInventory);

        restInventoryMockMvc.perform(put("/api/inventories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inventoryDTO)))
            .andExpect(status().isOk());

        // Validate the Inventory in the database
        List<Inventory> inventoryList = inventoryRepository.findAll();
        assertThat(inventoryList).hasSize(databaseSizeBeforeUpdate);
        Inventory testInventory = inventoryList.get(inventoryList.size() - 1);
        assertThat(testInventory.getVin()).isEqualTo(UPDATED_VIN);
        assertThat(testInventory.getYear()).isEqualTo(UPDATED_YEAR);
        assertThat(testInventory.getColor()).isEqualTo(UPDATED_COLOR);
        assertThat(testInventory.getComment()).isEqualTo(UPDATED_COMMENT);

        // Validate the Inventory in Elasticsearch
        verify(mockInventorySearchRepository, times(1)).save(testInventory);
    }

    @Test
    @Transactional
    public void updateNonExistingInventory() throws Exception {
        int databaseSizeBeforeUpdate = inventoryRepository.findAll().size();

        // Create the Inventory
        InventoryDTO inventoryDTO = inventoryMapper.toDto(inventory);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInventoryMockMvc.perform(put("/api/inventories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(inventoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Inventory in the database
        List<Inventory> inventoryList = inventoryRepository.findAll();
        assertThat(inventoryList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Inventory in Elasticsearch
        verify(mockInventorySearchRepository, times(0)).save(inventory);
    }

    @Test
    @Transactional
    public void deleteInventory() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);

        int databaseSizeBeforeDelete = inventoryRepository.findAll().size();

        // Delete the inventory
        restInventoryMockMvc.perform(delete("/api/inventories/{id}", inventory.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Inventory> inventoryList = inventoryRepository.findAll();
        assertThat(inventoryList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Inventory in Elasticsearch
        verify(mockInventorySearchRepository, times(1)).deleteById(inventory.getId());
    }

    @Test
    @Transactional
    public void searchInventory() throws Exception {
        // Initialize the database
        inventoryRepository.saveAndFlush(inventory);
        when(mockInventorySearchRepository.search(queryStringQuery("id:" + inventory.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(inventory), PageRequest.of(0, 1), 1));
        // Search the inventory
        restInventoryMockMvc.perform(get("/api/_search/inventories?query=id:" + inventory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(inventory.getId().intValue())))
            .andExpect(jsonPath("$.[*].vin").value(hasItem(DEFAULT_VIN)))
            .andExpect(jsonPath("$.[*].year").value(hasItem(DEFAULT_YEAR)))
            .andExpect(jsonPath("$.[*].color").value(hasItem(DEFAULT_COLOR)))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Inventory.class);
        Inventory inventory1 = new Inventory();
        inventory1.setId(1L);
        Inventory inventory2 = new Inventory();
        inventory2.setId(inventory1.getId());
        assertThat(inventory1).isEqualTo(inventory2);
        inventory2.setId(2L);
        assertThat(inventory1).isNotEqualTo(inventory2);
        inventory1.setId(null);
        assertThat(inventory1).isNotEqualTo(inventory2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(InventoryDTO.class);
        InventoryDTO inventoryDTO1 = new InventoryDTO();
        inventoryDTO1.setId(1L);
        InventoryDTO inventoryDTO2 = new InventoryDTO();
        assertThat(inventoryDTO1).isNotEqualTo(inventoryDTO2);
        inventoryDTO2.setId(inventoryDTO1.getId());
        assertThat(inventoryDTO1).isEqualTo(inventoryDTO2);
        inventoryDTO2.setId(2L);
        assertThat(inventoryDTO1).isNotEqualTo(inventoryDTO2);
        inventoryDTO1.setId(null);
        assertThat(inventoryDTO1).isNotEqualTo(inventoryDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(inventoryMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(inventoryMapper.fromId(null)).isNull();
    }
}
