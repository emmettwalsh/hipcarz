package com.hipcarz.web.rest;

import com.hipcarz.HipcarzApp;
import com.hipcarz.domain.Make;
import com.hipcarz.repository.MakeRepository;
import com.hipcarz.repository.search.MakeSearchRepository;
import com.hipcarz.service.MakeService;
import com.hipcarz.service.dto.MakeDTO;
import com.hipcarz.service.mapper.MakeMapper;
import com.hipcarz.web.rest.errors.ExceptionTranslator;
import com.hipcarz.service.dto.MakeCriteria;
import com.hipcarz.service.MakeQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

import static com.hipcarz.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.elasticsearch.index.query.QueryBuilders.queryStringQuery;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link MakeResource} REST controller.
 */
@SpringBootTest(classes = HipcarzApp.class)
public class MakeResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESC = "AAAAAAAAAA";
    private static final String UPDATED_DESC = "BBBBBBBBBB";

    @Autowired
    private MakeRepository makeRepository;

    @Autowired
    private MakeMapper makeMapper;

    @Autowired
    private MakeService makeService;

    /**
     * This repository is mocked in the com.hipcarz.repository.search test package.
     *
     * @see com.hipcarz.repository.search.MakeSearchRepositoryMockConfiguration
     */
    @Autowired
    private MakeSearchRepository mockMakeSearchRepository;

    @Autowired
    private MakeQueryService makeQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMakeMockMvc;

    private Make make;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MakeResource makeResource = new MakeResource(makeService, makeQueryService);
        this.restMakeMockMvc = MockMvcBuilders.standaloneSetup(makeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Make createEntity(EntityManager em) {
        Make make = new Make()
            .name(DEFAULT_NAME)
            .desc(DEFAULT_DESC);
        return make;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Make createUpdatedEntity(EntityManager em) {
        Make make = new Make()
            .name(UPDATED_NAME)
            .desc(UPDATED_DESC);
        return make;
    }

    @BeforeEach
    public void initTest() {
        make = createEntity(em);
    }

    @Test
    @Transactional
    public void createMake() throws Exception {
        int databaseSizeBeforeCreate = makeRepository.findAll().size();

        // Create the Make
        MakeDTO makeDTO = makeMapper.toDto(make);
        restMakeMockMvc.perform(post("/api/makes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(makeDTO)))
            .andExpect(status().isCreated());

        // Validate the Make in the database
        List<Make> makeList = makeRepository.findAll();
        assertThat(makeList).hasSize(databaseSizeBeforeCreate + 1);
        Make testMake = makeList.get(makeList.size() - 1);
        assertThat(testMake.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testMake.getDesc()).isEqualTo(DEFAULT_DESC);

        // Validate the Make in Elasticsearch
        verify(mockMakeSearchRepository, times(1)).save(testMake);
    }

    @Test
    @Transactional
    public void createMakeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = makeRepository.findAll().size();

        // Create the Make with an existing ID
        make.setId(1L);
        MakeDTO makeDTO = makeMapper.toDto(make);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMakeMockMvc.perform(post("/api/makes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(makeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Make in the database
        List<Make> makeList = makeRepository.findAll();
        assertThat(makeList).hasSize(databaseSizeBeforeCreate);

        // Validate the Make in Elasticsearch
        verify(mockMakeSearchRepository, times(0)).save(make);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = makeRepository.findAll().size();
        // set the field null
        make.setName(null);

        // Create the Make, which fails.
        MakeDTO makeDTO = makeMapper.toDto(make);

        restMakeMockMvc.perform(post("/api/makes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(makeDTO)))
            .andExpect(status().isBadRequest());

        List<Make> makeList = makeRepository.findAll();
        assertThat(makeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMakes() throws Exception {
        // Initialize the database
        makeRepository.saveAndFlush(make);

        // Get all the makeList
        restMakeMockMvc.perform(get("/api/makes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(make.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].desc").value(hasItem(DEFAULT_DESC.toString())));
    }
    
    @Test
    @Transactional
    public void getMake() throws Exception {
        // Initialize the database
        makeRepository.saveAndFlush(make);

        // Get the make
        restMakeMockMvc.perform(get("/api/makes/{id}", make.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(make.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.desc").value(DEFAULT_DESC.toString()));
    }

    @Test
    @Transactional
    public void getAllMakesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        makeRepository.saveAndFlush(make);

        // Get all the makeList where name equals to DEFAULT_NAME
        defaultMakeShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the makeList where name equals to UPDATED_NAME
        defaultMakeShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllMakesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        makeRepository.saveAndFlush(make);

        // Get all the makeList where name in DEFAULT_NAME or UPDATED_NAME
        defaultMakeShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the makeList where name equals to UPDATED_NAME
        defaultMakeShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllMakesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        makeRepository.saveAndFlush(make);

        // Get all the makeList where name is not null
        defaultMakeShouldBeFound("name.specified=true");

        // Get all the makeList where name is null
        defaultMakeShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllMakesByDescIsEqualToSomething() throws Exception {
        // Initialize the database
        makeRepository.saveAndFlush(make);

        // Get all the makeList where desc equals to DEFAULT_DESC
        defaultMakeShouldBeFound("desc.equals=" + DEFAULT_DESC);

        // Get all the makeList where desc equals to UPDATED_DESC
        defaultMakeShouldNotBeFound("desc.equals=" + UPDATED_DESC);
    }

    @Test
    @Transactional
    public void getAllMakesByDescIsInShouldWork() throws Exception {
        // Initialize the database
        makeRepository.saveAndFlush(make);

        // Get all the makeList where desc in DEFAULT_DESC or UPDATED_DESC
        defaultMakeShouldBeFound("desc.in=" + DEFAULT_DESC + "," + UPDATED_DESC);

        // Get all the makeList where desc equals to UPDATED_DESC
        defaultMakeShouldNotBeFound("desc.in=" + UPDATED_DESC);
    }

    @Test
    @Transactional
    public void getAllMakesByDescIsNullOrNotNull() throws Exception {
        // Initialize the database
        makeRepository.saveAndFlush(make);

        // Get all the makeList where desc is not null
        defaultMakeShouldBeFound("desc.specified=true");

        // Get all the makeList where desc is null
        defaultMakeShouldNotBeFound("desc.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultMakeShouldBeFound(String filter) throws Exception {
        restMakeMockMvc.perform(get("/api/makes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(make.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].desc").value(hasItem(DEFAULT_DESC)));

        // Check, that the count call also returns 1
        restMakeMockMvc.perform(get("/api/makes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultMakeShouldNotBeFound(String filter) throws Exception {
        restMakeMockMvc.perform(get("/api/makes?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restMakeMockMvc.perform(get("/api/makes/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingMake() throws Exception {
        // Get the make
        restMakeMockMvc.perform(get("/api/makes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMake() throws Exception {
        // Initialize the database
        makeRepository.saveAndFlush(make);

        int databaseSizeBeforeUpdate = makeRepository.findAll().size();

        // Update the make
        Make updatedMake = makeRepository.findById(make.getId()).get();
        // Disconnect from session so that the updates on updatedMake are not directly saved in db
        em.detach(updatedMake);
        updatedMake
            .name(UPDATED_NAME)
            .desc(UPDATED_DESC);
        MakeDTO makeDTO = makeMapper.toDto(updatedMake);

        restMakeMockMvc.perform(put("/api/makes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(makeDTO)))
            .andExpect(status().isOk());

        // Validate the Make in the database
        List<Make> makeList = makeRepository.findAll();
        assertThat(makeList).hasSize(databaseSizeBeforeUpdate);
        Make testMake = makeList.get(makeList.size() - 1);
        assertThat(testMake.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testMake.getDesc()).isEqualTo(UPDATED_DESC);

        // Validate the Make in Elasticsearch
        verify(mockMakeSearchRepository, times(1)).save(testMake);
    }

    @Test
    @Transactional
    public void updateNonExistingMake() throws Exception {
        int databaseSizeBeforeUpdate = makeRepository.findAll().size();

        // Create the Make
        MakeDTO makeDTO = makeMapper.toDto(make);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMakeMockMvc.perform(put("/api/makes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(makeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Make in the database
        List<Make> makeList = makeRepository.findAll();
        assertThat(makeList).hasSize(databaseSizeBeforeUpdate);

        // Validate the Make in Elasticsearch
        verify(mockMakeSearchRepository, times(0)).save(make);
    }

    @Test
    @Transactional
    public void deleteMake() throws Exception {
        // Initialize the database
        makeRepository.saveAndFlush(make);

        int databaseSizeBeforeDelete = makeRepository.findAll().size();

        // Delete the make
        restMakeMockMvc.perform(delete("/api/makes/{id}", make.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Make> makeList = makeRepository.findAll();
        assertThat(makeList).hasSize(databaseSizeBeforeDelete - 1);

        // Validate the Make in Elasticsearch
        verify(mockMakeSearchRepository, times(1)).deleteById(make.getId());
    }

    @Test
    @Transactional
    public void searchMake() throws Exception {
        // Initialize the database
        makeRepository.saveAndFlush(make);
        when(mockMakeSearchRepository.search(queryStringQuery("id:" + make.getId()), PageRequest.of(0, 20)))
            .thenReturn(new PageImpl<>(Collections.singletonList(make), PageRequest.of(0, 1), 1));
        // Search the make
        restMakeMockMvc.perform(get("/api/_search/makes?query=id:" + make.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(make.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].desc").value(hasItem(DEFAULT_DESC)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Make.class);
        Make make1 = new Make();
        make1.setId(1L);
        Make make2 = new Make();
        make2.setId(make1.getId());
        assertThat(make1).isEqualTo(make2);
        make2.setId(2L);
        assertThat(make1).isNotEqualTo(make2);
        make1.setId(null);
        assertThat(make1).isNotEqualTo(make2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MakeDTO.class);
        MakeDTO makeDTO1 = new MakeDTO();
        makeDTO1.setId(1L);
        MakeDTO makeDTO2 = new MakeDTO();
        assertThat(makeDTO1).isNotEqualTo(makeDTO2);
        makeDTO2.setId(makeDTO1.getId());
        assertThat(makeDTO1).isEqualTo(makeDTO2);
        makeDTO2.setId(2L);
        assertThat(makeDTO1).isNotEqualTo(makeDTO2);
        makeDTO1.setId(null);
        assertThat(makeDTO1).isNotEqualTo(makeDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(makeMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(makeMapper.fromId(null)).isNull();
    }
}
