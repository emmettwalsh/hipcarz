package com.hipcarz.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.hipcarz.domain.InventoryModel;
import com.hipcarz.domain.*; // for static metamodels
import com.hipcarz.repository.InventoryModelRepository;
import com.hipcarz.repository.search.InventoryModelSearchRepository;
import com.hipcarz.service.dto.InventoryModelCriteria;
import com.hipcarz.service.dto.InventoryModelDTO;
import com.hipcarz.service.mapper.InventoryModelMapper;

/**
 * Service for executing complex queries for {@link InventoryModel} entities in the database.
 * The main input is a {@link InventoryModelCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link InventoryModelDTO} or a {@link Page} of {@link InventoryModelDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class InventoryModelQueryService extends QueryService<InventoryModel> {

    private final Logger log = LoggerFactory.getLogger(InventoryModelQueryService.class);

    private final InventoryModelRepository inventoryModelRepository;

    private final InventoryModelMapper inventoryModelMapper;

    private final InventoryModelSearchRepository inventoryModelSearchRepository;

    public InventoryModelQueryService(InventoryModelRepository inventoryModelRepository, InventoryModelMapper inventoryModelMapper, InventoryModelSearchRepository inventoryModelSearchRepository) {
        this.inventoryModelRepository = inventoryModelRepository;
        this.inventoryModelMapper = inventoryModelMapper;
        this.inventoryModelSearchRepository = inventoryModelSearchRepository;
    }

    /**
     * Return a {@link List} of {@link InventoryModelDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<InventoryModelDTO> findByCriteria(InventoryModelCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<InventoryModel> specification = createSpecification(criteria);
        return inventoryModelMapper.toDto(inventoryModelRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link InventoryModelDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<InventoryModelDTO> findByCriteria(InventoryModelCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<InventoryModel> specification = createSpecification(criteria);
        return inventoryModelRepository.findAll(specification, page)
            .map(inventoryModelMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(InventoryModelCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<InventoryModel> specification = createSpecification(criteria);
        return inventoryModelRepository.count(specification);
    }

    /**
     * Function to convert InventoryModelCriteria to a {@link Specification}.
     */
    private Specification<InventoryModel> createSpecification(InventoryModelCriteria criteria) {
        Specification<InventoryModel> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), InventoryModel_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), InventoryModel_.name));
            }
            if (criteria.getVehicleType() != null) {
                specification = specification.and(buildSpecification(criteria.getVehicleType(), InventoryModel_.vehicleType));
            }
            if (criteria.getDesc() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDesc(), InventoryModel_.desc));
            }
            if (criteria.getMakeId() != null) {
                specification = specification.and(buildSpecification(criteria.getMakeId(),
                    root -> root.join(InventoryModel_.make, JoinType.LEFT).get(Make_.id)));
            }
        }
        return specification;
    }
}
