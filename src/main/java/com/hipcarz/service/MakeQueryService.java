package com.hipcarz.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.hipcarz.domain.Make;
import com.hipcarz.domain.*; // for static metamodels
import com.hipcarz.repository.MakeRepository;
import com.hipcarz.repository.search.MakeSearchRepository;
import com.hipcarz.service.dto.MakeCriteria;
import com.hipcarz.service.dto.MakeDTO;
import com.hipcarz.service.mapper.MakeMapper;

/**
 * Service for executing complex queries for {@link Make} entities in the database.
 * The main input is a {@link MakeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MakeDTO} or a {@link Page} of {@link MakeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MakeQueryService extends QueryService<Make> {

    private final Logger log = LoggerFactory.getLogger(MakeQueryService.class);

    private final MakeRepository makeRepository;

    private final MakeMapper makeMapper;

    private final MakeSearchRepository makeSearchRepository;

    public MakeQueryService(MakeRepository makeRepository, MakeMapper makeMapper, MakeSearchRepository makeSearchRepository) {
        this.makeRepository = makeRepository;
        this.makeMapper = makeMapper;
        this.makeSearchRepository = makeSearchRepository;
    }

    /**
     * Return a {@link List} of {@link MakeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MakeDTO> findByCriteria(MakeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Make> specification = createSpecification(criteria);
        return makeMapper.toDto(makeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link MakeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MakeDTO> findByCriteria(MakeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Make> specification = createSpecification(criteria);
        return makeRepository.findAll(specification, page)
            .map(makeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(MakeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Make> specification = createSpecification(criteria);
        return makeRepository.count(specification);
    }

    /**
     * Function to convert MakeCriteria to a {@link Specification}.
     */
    private Specification<Make> createSpecification(MakeCriteria criteria) {
        Specification<Make> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Make_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Make_.name));
            }
            if (criteria.getDesc() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDesc(), Make_.desc));
            }
        }
        return specification;
    }
}
