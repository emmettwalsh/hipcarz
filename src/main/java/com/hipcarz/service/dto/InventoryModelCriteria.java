package com.hipcarz.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import com.hipcarz.domain.enumeration.VehicleType;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.hipcarz.domain.InventoryModel} entity. This class is used
 * in {@link com.hipcarz.web.rest.InventoryModelResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /inventory-models?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class InventoryModelCriteria implements Serializable, Criteria {
    /**
     * Class for filtering VehicleType
     */
    public static class VehicleTypeFilter extends Filter<VehicleType> {

        public VehicleTypeFilter() {
        }

        public VehicleTypeFilter(VehicleTypeFilter filter) {
            super(filter);
        }

        @Override
        public VehicleTypeFilter copy() {
            return new VehicleTypeFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private VehicleTypeFilter vehicleType;

    private StringFilter desc;

    private LongFilter makeId;

    public InventoryModelCriteria(){
    }

    public InventoryModelCriteria(InventoryModelCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.vehicleType = other.vehicleType == null ? null : other.vehicleType.copy();
        this.desc = other.desc == null ? null : other.desc.copy();
        this.makeId = other.makeId == null ? null : other.makeId.copy();
    }

    @Override
    public InventoryModelCriteria copy() {
        return new InventoryModelCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public VehicleTypeFilter getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleTypeFilter vehicleType) {
        this.vehicleType = vehicleType;
    }

    public StringFilter getDesc() {
        return desc;
    }

    public void setDesc(StringFilter desc) {
        this.desc = desc;
    }

    public LongFilter getMakeId() {
        return makeId;
    }

    public void setMakeId(LongFilter makeId) {
        this.makeId = makeId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final InventoryModelCriteria that = (InventoryModelCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(vehicleType, that.vehicleType) &&
            Objects.equals(desc, that.desc) &&
            Objects.equals(makeId, that.makeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        name,
        vehicleType,
        desc,
        makeId
        );
    }

    @Override
    public String toString() {
        return "InventoryModelCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (vehicleType != null ? "vehicleType=" + vehicleType + ", " : "") +
                (desc != null ? "desc=" + desc + ", " : "") +
                (makeId != null ? "makeId=" + makeId + ", " : "") +
            "}";
    }

}
