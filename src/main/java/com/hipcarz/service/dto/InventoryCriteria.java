package com.hipcarz.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.hipcarz.domain.Inventory} entity. This class is used
 * in {@link com.hipcarz.web.rest.InventoryResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /inventories?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class InventoryCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter vin;

    private IntegerFilter year;

    private StringFilter color;

    private StringFilter comment;

    private LongFilter inventoryModelId;

    public InventoryCriteria(){
    }

    public InventoryCriteria(InventoryCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.vin = other.vin == null ? null : other.vin.copy();
        this.year = other.year == null ? null : other.year.copy();
        this.color = other.color == null ? null : other.color.copy();
        this.comment = other.comment == null ? null : other.comment.copy();
        this.inventoryModelId = other.inventoryModelId == null ? null : other.inventoryModelId.copy();
    }

    @Override
    public InventoryCriteria copy() {
        return new InventoryCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getVin() {
        return vin;
    }

    public void setVin(StringFilter vin) {
        this.vin = vin;
    }

    public IntegerFilter getYear() {
        return year;
    }

    public void setYear(IntegerFilter year) {
        this.year = year;
    }

    public StringFilter getColor() {
        return color;
    }

    public void setColor(StringFilter color) {
        this.color = color;
    }

    public StringFilter getComment() {
        return comment;
    }

    public void setComment(StringFilter comment) {
        this.comment = comment;
    }

    public LongFilter getInventoryModelId() {
        return inventoryModelId;
    }

    public void setInventoryModelId(LongFilter inventoryModelId) {
        this.inventoryModelId = inventoryModelId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final InventoryCriteria that = (InventoryCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(vin, that.vin) &&
            Objects.equals(year, that.year) &&
            Objects.equals(color, that.color) &&
            Objects.equals(comment, that.comment) &&
            Objects.equals(inventoryModelId, that.inventoryModelId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        vin,
        year,
        color,
        comment,
        inventoryModelId
        );
    }

    @Override
    public String toString() {
        return "InventoryCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (vin != null ? "vin=" + vin + ", " : "") +
                (year != null ? "year=" + year + ", " : "") +
                (color != null ? "color=" + color + ", " : "") +
                (comment != null ? "comment=" + comment + ", " : "") +
                (inventoryModelId != null ? "inventoryModelId=" + inventoryModelId + ", " : "") +
            "}";
    }

}
