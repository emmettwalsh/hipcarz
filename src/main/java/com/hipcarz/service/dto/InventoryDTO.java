package com.hipcarz.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.hipcarz.domain.Inventory} entity.
 */
public class InventoryDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 17)
    private String vin;

    @Min(value = 1900)
    @Max(value = 2050)
    private Integer year;

    private String color;

    @Size(max = 128)
    private String comment;


    private Long inventoryModelId;

    private String inventoryModelName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getInventoryModelId() {
        return inventoryModelId;
    }

    public void setInventoryModelId(Long inventoryModelId) {
        this.inventoryModelId = inventoryModelId;
    }

    public String getInventoryModelName() {
        return inventoryModelName;
    }

    public void setInventoryModelName(String inventoryModelName) {
        this.inventoryModelName = inventoryModelName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InventoryDTO inventoryDTO = (InventoryDTO) o;
        if (inventoryDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), inventoryDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InventoryDTO{" +
            "id=" + getId() +
            ", vin='" + getVin() + "'" +
            ", year=" + getYear() +
            ", color='" + getColor() + "'" +
            ", comment='" + getComment() + "'" +
            ", inventoryModel=" + getInventoryModelId() +
            ", inventoryModel='" + getInventoryModelName() + "'" +
            "}";
    }
}
