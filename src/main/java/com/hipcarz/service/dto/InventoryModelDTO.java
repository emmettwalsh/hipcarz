package com.hipcarz.service.dto;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import com.hipcarz.domain.enumeration.VehicleType;

/**
 * A DTO for the {@link com.hipcarz.domain.InventoryModel} entity.
 */
public class InventoryModelDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(max = 20)
    private String name;

    private VehicleType vehicleType;

    private String desc;


    private Long makeId;

    private String makeName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Long getMakeId() {
        return makeId;
    }

    public void setMakeId(Long makeId) {
        this.makeId = makeId;
    }

    public String getMakeName() {
        return makeName;
    }

    public void setMakeName(String makeName) {
        this.makeName = makeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        InventoryModelDTO inventoryModelDTO = (InventoryModelDTO) o;
        if (inventoryModelDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), inventoryModelDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InventoryModelDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", vehicleType='" + getVehicleType() + "'" +
            ", desc='" + getDesc() + "'" +
            ", make=" + getMakeId() +
            ", make='" + getMakeName() + "'" +
            "}";
    }
}
