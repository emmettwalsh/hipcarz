package com.hipcarz.service;

import com.hipcarz.domain.Inventory;
import com.hipcarz.repository.InventoryRepository;
import com.hipcarz.repository.search.InventorySearchRepository;
import com.hipcarz.service.dto.InventoryDTO;
import com.hipcarz.service.mapper.InventoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Inventory}.
 */
@Service
@Transactional
public class InventoryService {

    private final Logger log = LoggerFactory.getLogger(InventoryService.class);

    private final InventoryRepository inventoryRepository;

    private final InventoryMapper inventoryMapper;

    private final InventorySearchRepository inventorySearchRepository;

    public InventoryService(InventoryRepository inventoryRepository, InventoryMapper inventoryMapper, InventorySearchRepository inventorySearchRepository) {
        this.inventoryRepository = inventoryRepository;
        this.inventoryMapper = inventoryMapper;
        this.inventorySearchRepository = inventorySearchRepository;
    }

    /**
     * Save a inventory.
     *
     * @param inventoryDTO the entity to save.
     * @return the persisted entity.
     */
    public InventoryDTO save(InventoryDTO inventoryDTO) {
        log.debug("Request to save Inventory : {}", inventoryDTO);
        Inventory inventory = inventoryMapper.toEntity(inventoryDTO);
        inventory = inventoryRepository.save(inventory);
        InventoryDTO result = inventoryMapper.toDto(inventory);
        inventorySearchRepository.save(inventory);
        return result;
    }

    /**
     * Get all the inventories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<InventoryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Inventories");
        return inventoryRepository.findAll(pageable)
            .map(inventoryMapper::toDto);
    }


    /**
     * Get one inventory by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<InventoryDTO> findOne(Long id) {
        log.debug("Request to get Inventory : {}", id);
        return inventoryRepository.findById(id)
            .map(inventoryMapper::toDto);
    }

    /**
     * Delete the inventory by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Inventory : {}", id);
        inventoryRepository.deleteById(id);
        inventorySearchRepository.deleteById(id);
    }

    /**
     * Search for the inventory corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<InventoryDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Inventories for query {}", query);
        return inventorySearchRepository.search(queryStringQuery(query), pageable)
            .map(inventoryMapper::toDto);
    }
}
