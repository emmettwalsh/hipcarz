package com.hipcarz.service;

import com.hipcarz.domain.Make;
import com.hipcarz.repository.MakeRepository;
import com.hipcarz.repository.search.MakeSearchRepository;
import com.hipcarz.service.dto.MakeDTO;
import com.hipcarz.service.mapper.MakeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link Make}.
 */
@Service
@Transactional
public class MakeService {

    private final Logger log = LoggerFactory.getLogger(MakeService.class);

    private final MakeRepository makeRepository;

    private final MakeMapper makeMapper;

    private final MakeSearchRepository makeSearchRepository;

    public MakeService(MakeRepository makeRepository, MakeMapper makeMapper, MakeSearchRepository makeSearchRepository) {
        this.makeRepository = makeRepository;
        this.makeMapper = makeMapper;
        this.makeSearchRepository = makeSearchRepository;
    }

    /**
     * Save a make.
     *
     * @param makeDTO the entity to save.
     * @return the persisted entity.
     */
    public MakeDTO save(MakeDTO makeDTO) {
        log.debug("Request to save Make : {}", makeDTO);
        Make make = makeMapper.toEntity(makeDTO);
        make = makeRepository.save(make);
        MakeDTO result = makeMapper.toDto(make);
        makeSearchRepository.save(make);
        return result;
    }

    /**
     * Get all the makes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<MakeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Makes");
        return makeRepository.findAll(pageable)
            .map(makeMapper::toDto);
    }


    /**
     * Get one make by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<MakeDTO> findOne(Long id) {
        log.debug("Request to get Make : {}", id);
        return makeRepository.findById(id)
            .map(makeMapper::toDto);
    }

    /**
     * Delete the make by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Make : {}", id);
        makeRepository.deleteById(id);
        makeSearchRepository.deleteById(id);
    }

    /**
     * Search for the make corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<MakeDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Makes for query {}", query);
        return makeSearchRepository.search(queryStringQuery(query), pageable)
            .map(makeMapper::toDto);
    }
}
