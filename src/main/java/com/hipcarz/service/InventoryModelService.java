package com.hipcarz.service;

import com.hipcarz.domain.InventoryModel;
import com.hipcarz.repository.InventoryModelRepository;
import com.hipcarz.repository.search.InventoryModelSearchRepository;
import com.hipcarz.service.dto.InventoryModelDTO;
import com.hipcarz.service.mapper.InventoryModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing {@link InventoryModel}.
 */
@Service
@Transactional
public class InventoryModelService {

    private final Logger log = LoggerFactory.getLogger(InventoryModelService.class);

    private final InventoryModelRepository inventoryModelRepository;

    private final InventoryModelMapper inventoryModelMapper;

    private final InventoryModelSearchRepository inventoryModelSearchRepository;

    public InventoryModelService(InventoryModelRepository inventoryModelRepository, InventoryModelMapper inventoryModelMapper, InventoryModelSearchRepository inventoryModelSearchRepository) {
        this.inventoryModelRepository = inventoryModelRepository;
        this.inventoryModelMapper = inventoryModelMapper;
        this.inventoryModelSearchRepository = inventoryModelSearchRepository;
    }

    /**
     * Save a inventoryModel.
     *
     * @param inventoryModelDTO the entity to save.
     * @return the persisted entity.
     */
    public InventoryModelDTO save(InventoryModelDTO inventoryModelDTO) {
        log.debug("Request to save InventoryModel : {}", inventoryModelDTO);
        InventoryModel inventoryModel = inventoryModelMapper.toEntity(inventoryModelDTO);
        inventoryModel = inventoryModelRepository.save(inventoryModel);
        InventoryModelDTO result = inventoryModelMapper.toDto(inventoryModel);
        inventoryModelSearchRepository.save(inventoryModel);
        return result;
    }

    /**
     * Get all the inventoryModels.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<InventoryModelDTO> findAll(Pageable pageable) {
        log.debug("Request to get all InventoryModels");
        return inventoryModelRepository.findAll(pageable)
            .map(inventoryModelMapper::toDto);
    }


    /**
     * Get one inventoryModel by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<InventoryModelDTO> findOne(Long id) {
        log.debug("Request to get InventoryModel : {}", id);
        return inventoryModelRepository.findById(id)
            .map(inventoryModelMapper::toDto);
    }

    /**
     * Delete the inventoryModel by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete InventoryModel : {}", id);
        inventoryModelRepository.deleteById(id);
        inventoryModelSearchRepository.deleteById(id);
    }

    /**
     * Search for the inventoryModel corresponding to the query.
     *
     * @param query the query of the search.
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<InventoryModelDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of InventoryModels for query {}", query);
        return inventoryModelSearchRepository.search(queryStringQuery(query), pageable)
            .map(inventoryModelMapper::toDto);
    }
}
