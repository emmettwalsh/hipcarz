package com.hipcarz.service.mapper;

import com.hipcarz.domain.*;
import com.hipcarz.service.dto.InventoryModelDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link InventoryModel} and its DTO {@link InventoryModelDTO}.
 */
@Mapper(componentModel = "spring", uses = {MakeMapper.class})
public interface InventoryModelMapper extends EntityMapper<InventoryModelDTO, InventoryModel> {

    @Mapping(source = "make.id", target = "makeId")
    @Mapping(source = "make.name", target = "makeName")
    InventoryModelDTO toDto(InventoryModel inventoryModel);

    @Mapping(source = "makeId", target = "make")
    InventoryModel toEntity(InventoryModelDTO inventoryModelDTO);

    default InventoryModel fromId(Long id) {
        if (id == null) {
            return null;
        }
        InventoryModel inventoryModel = new InventoryModel();
        inventoryModel.setId(id);
        return inventoryModel;
    }
}
