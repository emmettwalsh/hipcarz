package com.hipcarz.service.mapper;

import com.hipcarz.domain.*;
import com.hipcarz.service.dto.MakeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Make} and its DTO {@link MakeDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MakeMapper extends EntityMapper<MakeDTO, Make> {



    default Make fromId(Long id) {
        if (id == null) {
            return null;
        }
        Make make = new Make();
        make.setId(id);
        return make;
    }
}
