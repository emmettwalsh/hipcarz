package com.hipcarz.service.mapper;

import com.hipcarz.domain.*;
import com.hipcarz.service.dto.InventoryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Inventory} and its DTO {@link InventoryDTO}.
 */
@Mapper(componentModel = "spring", uses = {InventoryModelMapper.class})
public interface InventoryMapper extends EntityMapper<InventoryDTO, Inventory> {

    @Mapping(source = "inventoryModel.id", target = "inventoryModelId")
    @Mapping(source = "inventoryModel.name", target = "inventoryModelName")
    InventoryDTO toDto(Inventory inventory);

    @Mapping(source = "inventoryModelId", target = "inventoryModel")
    Inventory toEntity(InventoryDTO inventoryDTO);

    default Inventory fromId(Long id) {
        if (id == null) {
            return null;
        }
        Inventory inventory = new Inventory();
        inventory.setId(id);
        return inventory;
    }
}
