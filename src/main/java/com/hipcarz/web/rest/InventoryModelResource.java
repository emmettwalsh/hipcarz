package com.hipcarz.web.rest;

import com.hipcarz.service.InventoryModelService;
import com.hipcarz.web.rest.errors.BadRequestAlertException;
import com.hipcarz.service.dto.InventoryModelDTO;
import com.hipcarz.service.dto.InventoryModelCriteria;
import com.hipcarz.service.InventoryModelQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.hipcarz.domain.InventoryModel}.
 */
@RestController
@RequestMapping("/api")
public class InventoryModelResource {

    private final Logger log = LoggerFactory.getLogger(InventoryModelResource.class);

    private static final String ENTITY_NAME = "inventoryModel";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final InventoryModelService inventoryModelService;

    private final InventoryModelQueryService inventoryModelQueryService;

    public InventoryModelResource(InventoryModelService inventoryModelService, InventoryModelQueryService inventoryModelQueryService) {
        this.inventoryModelService = inventoryModelService;
        this.inventoryModelQueryService = inventoryModelQueryService;
    }

    /**
     * {@code POST  /inventory-models} : Create a new inventoryModel.
     *
     * @param inventoryModelDTO the inventoryModelDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new inventoryModelDTO, or with status {@code 400 (Bad Request)} if the inventoryModel has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/inventory-models")
    public ResponseEntity<InventoryModelDTO> createInventoryModel(@Valid @RequestBody InventoryModelDTO inventoryModelDTO) throws URISyntaxException {
        log.debug("REST request to save InventoryModel : {}", inventoryModelDTO);
        if (inventoryModelDTO.getId() != null) {
            throw new BadRequestAlertException("A new inventoryModel cannot already have an ID", ENTITY_NAME, "idexists");
        }
        InventoryModelDTO result = inventoryModelService.save(inventoryModelDTO);
        return ResponseEntity.created(new URI("/api/inventory-models/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /inventory-models} : Updates an existing inventoryModel.
     *
     * @param inventoryModelDTO the inventoryModelDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated inventoryModelDTO,
     * or with status {@code 400 (Bad Request)} if the inventoryModelDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the inventoryModelDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/inventory-models")
    public ResponseEntity<InventoryModelDTO> updateInventoryModel(@Valid @RequestBody InventoryModelDTO inventoryModelDTO) throws URISyntaxException {
        log.debug("REST request to update InventoryModel : {}", inventoryModelDTO);
        if (inventoryModelDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        InventoryModelDTO result = inventoryModelService.save(inventoryModelDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, inventoryModelDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /inventory-models} : get all the inventoryModels.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of inventoryModels in body.
     */
    @GetMapping("/inventory-models")
    public ResponseEntity<List<InventoryModelDTO>> getAllInventoryModels(InventoryModelCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get InventoryModels by criteria: {}", criteria);
        Page<InventoryModelDTO> page = inventoryModelQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /inventory-models/count} : count all the inventoryModels.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/inventory-models/count")
    public ResponseEntity<Long> countInventoryModels(InventoryModelCriteria criteria) {
        log.debug("REST request to count InventoryModels by criteria: {}", criteria);
        return ResponseEntity.ok().body(inventoryModelQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /inventory-models/:id} : get the "id" inventoryModel.
     *
     * @param id the id of the inventoryModelDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the inventoryModelDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/inventory-models/{id}")
    public ResponseEntity<InventoryModelDTO> getInventoryModel(@PathVariable Long id) {
        log.debug("REST request to get InventoryModel : {}", id);
        Optional<InventoryModelDTO> inventoryModelDTO = inventoryModelService.findOne(id);
        return ResponseUtil.wrapOrNotFound(inventoryModelDTO);
    }

    /**
     * {@code DELETE  /inventory-models/:id} : delete the "id" inventoryModel.
     *
     * @param id the id of the inventoryModelDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/inventory-models/{id}")
    public ResponseEntity<Void> deleteInventoryModel(@PathVariable Long id) {
        log.debug("REST request to delete InventoryModel : {}", id);
        inventoryModelService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/inventory-models?query=:query} : search for the inventoryModel corresponding
     * to the query.
     *
     * @param query the query of the inventoryModel search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/inventory-models")
    public ResponseEntity<List<InventoryModelDTO>> searchInventoryModels(@RequestParam String query, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to search for a page of InventoryModels for query {}", query);
        Page<InventoryModelDTO> page = inventoryModelService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
