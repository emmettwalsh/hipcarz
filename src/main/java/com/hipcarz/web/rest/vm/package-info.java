/**
 * View Models used by Spring MVC REST controllers.
 */
package com.hipcarz.web.rest.vm;
