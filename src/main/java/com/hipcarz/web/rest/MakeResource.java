package com.hipcarz.web.rest;

import com.hipcarz.service.MakeService;
import com.hipcarz.web.rest.errors.BadRequestAlertException;
import com.hipcarz.service.dto.MakeDTO;
import com.hipcarz.service.dto.MakeCriteria;
import com.hipcarz.service.MakeQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing {@link com.hipcarz.domain.Make}.
 */
@RestController
@RequestMapping("/api")
public class MakeResource {

    private final Logger log = LoggerFactory.getLogger(MakeResource.class);

    private static final String ENTITY_NAME = "make";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MakeService makeService;

    private final MakeQueryService makeQueryService;

    public MakeResource(MakeService makeService, MakeQueryService makeQueryService) {
        this.makeService = makeService;
        this.makeQueryService = makeQueryService;
    }

    /**
     * {@code POST  /makes} : Create a new make.
     *
     * @param makeDTO the makeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new makeDTO, or with status {@code 400 (Bad Request)} if the make has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/makes")
    public ResponseEntity<MakeDTO> createMake(@Valid @RequestBody MakeDTO makeDTO) throws URISyntaxException {
        log.debug("REST request to save Make : {}", makeDTO);
        if (makeDTO.getId() != null) {
            throw new BadRequestAlertException("A new make cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MakeDTO result = makeService.save(makeDTO);
        return ResponseEntity.created(new URI("/api/makes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /makes} : Updates an existing make.
     *
     * @param makeDTO the makeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated makeDTO,
     * or with status {@code 400 (Bad Request)} if the makeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the makeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/makes")
    public ResponseEntity<MakeDTO> updateMake(@Valid @RequestBody MakeDTO makeDTO) throws URISyntaxException {
        log.debug("REST request to update Make : {}", makeDTO);
        if (makeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MakeDTO result = makeService.save(makeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, makeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /makes} : get all the makes.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of makes in body.
     */
    @GetMapping("/makes")
    public ResponseEntity<List<MakeDTO>> getAllMakes(MakeCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get Makes by criteria: {}", criteria);
        Page<MakeDTO> page = makeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /makes/count} : count all the makes.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/makes/count")
    public ResponseEntity<Long> countMakes(MakeCriteria criteria) {
        log.debug("REST request to count Makes by criteria: {}", criteria);
        return ResponseEntity.ok().body(makeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /makes/:id} : get the "id" make.
     *
     * @param id the id of the makeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the makeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/makes/{id}")
    public ResponseEntity<MakeDTO> getMake(@PathVariable Long id) {
        log.debug("REST request to get Make : {}", id);
        Optional<MakeDTO> makeDTO = makeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(makeDTO);
    }

    /**
     * {@code DELETE  /makes/:id} : delete the "id" make.
     *
     * @param id the id of the makeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/makes/{id}")
    public ResponseEntity<Void> deleteMake(@PathVariable Long id) {
        log.debug("REST request to delete Make : {}", id);
        makeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code SEARCH  /_search/makes?query=:query} : search for the make corresponding
     * to the query.
     *
     * @param query the query of the make search.
     * @param pageable the pagination information.
     * @return the result of the search.
     */
    @GetMapping("/_search/makes")
    public ResponseEntity<List<MakeDTO>> searchMakes(@RequestParam String query, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to search for a page of Makes for query {}", query);
        Page<MakeDTO> page = makeService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

}
