package com.hipcarz.repository.search;

import com.hipcarz.domain.InventoryModel;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link InventoryModel} entity.
 */
public interface InventoryModelSearchRepository extends ElasticsearchRepository<InventoryModel, Long> {
}
