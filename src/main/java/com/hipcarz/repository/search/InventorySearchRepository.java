package com.hipcarz.repository.search;

import com.hipcarz.domain.Inventory;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Inventory} entity.
 */
public interface InventorySearchRepository extends ElasticsearchRepository<Inventory, Long> {
}
