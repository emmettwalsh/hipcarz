package com.hipcarz.repository.search;

import com.hipcarz.domain.Make;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the {@link Make} entity.
 */
public interface MakeSearchRepository extends ElasticsearchRepository<Make, Long> {
}
