package com.hipcarz.repository;

import com.hipcarz.domain.InventoryModel;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the InventoryModel entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InventoryModelRepository extends JpaRepository<InventoryModel, Long>, JpaSpecificationExecutor<InventoryModel> {

}
