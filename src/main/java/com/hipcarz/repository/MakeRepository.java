package com.hipcarz.repository;

import com.hipcarz.domain.Make;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Make entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MakeRepository extends JpaRepository<Make, Long>, JpaSpecificationExecutor<Make> {

}
