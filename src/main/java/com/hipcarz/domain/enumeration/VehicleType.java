package com.hipcarz.domain.enumeration;

/**
 * The VehicleType enumeration.
 */
public enum VehicleType {
    SUV, SEDAN, TRUCK, COUPE, CONVERTABLE, WAGON, HATCHBACK, MINIVAN
}
