import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IInventory, Inventory } from 'app/shared/model/inventory.model';
import { InventoryService } from './inventory.service';
import { IInventoryModel } from 'app/shared/model/inventory-model.model';
import { InventoryModelService } from 'app/entities/inventory-model';

@Component({
  selector: 'jhi-inventory-update',
  templateUrl: './inventory-update.component.html'
})
export class InventoryUpdateComponent implements OnInit {
  isSaving: boolean;

  inventorymodels: IInventoryModel[];

  editForm = this.fb.group({
    id: [],
    vin: [null, [Validators.required, Validators.maxLength(17)]],
    year: [null, [Validators.min(1900), Validators.max(2050)]],
    color: [],
    comment: [null, [Validators.maxLength(128)]],
    inventoryModelId: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected inventoryService: InventoryService,
    protected inventoryModelService: InventoryModelService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ inventory }) => {
      this.updateForm(inventory);
    });
    this.inventoryModelService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IInventoryModel[]>) => mayBeOk.ok),
        map((response: HttpResponse<IInventoryModel[]>) => response.body)
      )
      .subscribe((res: IInventoryModel[]) => (this.inventorymodels = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(inventory: IInventory) {
    this.editForm.patchValue({
      id: inventory.id,
      vin: inventory.vin,
      year: inventory.year,
      color: inventory.color,
      comment: inventory.comment,
      inventoryModelId: inventory.inventoryModelId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const inventory = this.createFromForm();
    if (inventory.id !== undefined) {
      this.subscribeToSaveResponse(this.inventoryService.update(inventory));
    } else {
      this.subscribeToSaveResponse(this.inventoryService.create(inventory));
    }
  }

  private createFromForm(): IInventory {
    const entity = {
      ...new Inventory(),
      id: this.editForm.get(['id']).value,
      vin: this.editForm.get(['vin']).value,
      year: this.editForm.get(['year']).value,
      color: this.editForm.get(['color']).value,
      comment: this.editForm.get(['comment']).value,
      inventoryModelId: this.editForm.get(['inventoryModelId']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IInventory>>) {
    result.subscribe((res: HttpResponse<IInventory>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackInventoryModelById(index: number, item: IInventoryModel) {
    return item.id;
  }
}
