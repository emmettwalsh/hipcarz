import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'make',
        loadChildren: './make/make.module#HipcarzMakeModule'
      },
      {
        path: 'inventory-model',
        loadChildren: './inventory-model/inventory-model.module#HipcarzInventoryModelModule'
      },
      {
        path: 'inventory',
        loadChildren: './inventory/inventory.module#HipcarzInventoryModule'
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HipcarzEntityModule {}
