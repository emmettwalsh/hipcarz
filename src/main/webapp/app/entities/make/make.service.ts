import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IMake } from 'app/shared/model/make.model';

type EntityResponseType = HttpResponse<IMake>;
type EntityArrayResponseType = HttpResponse<IMake[]>;

@Injectable({ providedIn: 'root' })
export class MakeService {
  public resourceUrl = SERVER_API_URL + 'api/makes';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/makes';

  constructor(protected http: HttpClient) {}

  create(make: IMake): Observable<EntityResponseType> {
    return this.http.post<IMake>(this.resourceUrl, make, { observe: 'response' });
  }

  update(make: IMake): Observable<EntityResponseType> {
    return this.http.put<IMake>(this.resourceUrl, make, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMake>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMake[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMake[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
