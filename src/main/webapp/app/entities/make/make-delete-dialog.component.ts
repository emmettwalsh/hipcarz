import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMake } from 'app/shared/model/make.model';
import { MakeService } from './make.service';

@Component({
  selector: 'jhi-make-delete-dialog',
  templateUrl: './make-delete-dialog.component.html'
})
export class MakeDeleteDialogComponent {
  make: IMake;

  constructor(protected makeService: MakeService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.makeService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'makeListModification',
        content: 'Deleted an make'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-make-delete-popup',
  template: ''
})
export class MakeDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ make }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(MakeDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.make = make;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/make', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/make', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
