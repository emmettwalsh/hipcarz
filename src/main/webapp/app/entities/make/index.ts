export * from './make.service';
export * from './make-update.component';
export * from './make-delete-dialog.component';
export * from './make-detail.component';
export * from './make.component';
export * from './make.route';
