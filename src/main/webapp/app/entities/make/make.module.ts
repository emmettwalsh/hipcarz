import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { HipcarzSharedModule } from 'app/shared';
import {
  MakeComponent,
  MakeDetailComponent,
  MakeUpdateComponent,
  MakeDeletePopupComponent,
  MakeDeleteDialogComponent,
  makeRoute,
  makePopupRoute
} from './';

const ENTITY_STATES = [...makeRoute, ...makePopupRoute];

@NgModule({
  imports: [HipcarzSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [MakeComponent, MakeDetailComponent, MakeUpdateComponent, MakeDeleteDialogComponent, MakeDeletePopupComponent],
  entryComponents: [MakeComponent, MakeUpdateComponent, MakeDeleteDialogComponent, MakeDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HipcarzMakeModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
