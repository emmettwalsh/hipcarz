import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Make } from 'app/shared/model/make.model';
import { MakeService } from './make.service';
import { MakeComponent } from './make.component';
import { MakeDetailComponent } from './make-detail.component';
import { MakeUpdateComponent } from './make-update.component';
import { MakeDeletePopupComponent } from './make-delete-dialog.component';
import { IMake } from 'app/shared/model/make.model';

@Injectable({ providedIn: 'root' })
export class MakeResolve implements Resolve<IMake> {
  constructor(private service: MakeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMake> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Make>) => response.ok),
        map((make: HttpResponse<Make>) => make.body)
      );
    }
    return of(new Make());
  }
}

export const makeRoute: Routes = [
  {
    path: '',
    component: MakeComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'hipcarzApp.make.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: MakeDetailComponent,
    resolve: {
      make: MakeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hipcarzApp.make.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: MakeUpdateComponent,
    resolve: {
      make: MakeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hipcarzApp.make.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: MakeUpdateComponent,
    resolve: {
      make: MakeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hipcarzApp.make.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const makePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: MakeDeletePopupComponent,
    resolve: {
      make: MakeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hipcarzApp.make.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
