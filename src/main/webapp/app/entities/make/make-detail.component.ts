import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMake } from 'app/shared/model/make.model';

@Component({
  selector: 'jhi-make-detail',
  templateUrl: './make-detail.component.html'
})
export class MakeDetailComponent implements OnInit {
  make: IMake;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ make }) => {
      this.make = make;
    });
  }

  previousState() {
    window.history.back();
  }
}
