import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IMake, Make } from 'app/shared/model/make.model';
import { MakeService } from './make.service';

@Component({
  selector: 'jhi-make-update',
  templateUrl: './make-update.component.html'
})
export class MakeUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.maxLength(20)]],
    desc: []
  });

  constructor(protected makeService: MakeService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ make }) => {
      this.updateForm(make);
    });
  }

  updateForm(make: IMake) {
    this.editForm.patchValue({
      id: make.id,
      name: make.name,
      desc: make.desc
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const make = this.createFromForm();
    if (make.id !== undefined) {
      this.subscribeToSaveResponse(this.makeService.update(make));
    } else {
      this.subscribeToSaveResponse(this.makeService.create(make));
    }
  }

  private createFromForm(): IMake {
    const entity = {
      ...new Make(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      desc: this.editForm.get(['desc']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMake>>) {
    result.subscribe((res: HttpResponse<IMake>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
