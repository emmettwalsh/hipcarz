import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IInventoryModel, InventoryModel } from 'app/shared/model/inventory-model.model';
import { InventoryModelService } from './inventory-model.service';
import { IMake } from 'app/shared/model/make.model';
import { MakeService } from 'app/entities/make';

@Component({
  selector: 'jhi-inventory-model-update',
  templateUrl: './inventory-model-update.component.html'
})
export class InventoryModelUpdateComponent implements OnInit {
  isSaving: boolean;

  makes: IMake[];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.maxLength(20)]],
    vehicleType: [],
    desc: [],
    makeId: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected inventoryModelService: InventoryModelService,
    protected makeService: MakeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ inventoryModel }) => {
      this.updateForm(inventoryModel);
    });
    this.makeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMake[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMake[]>) => response.body)
      )
      .subscribe((res: IMake[]) => (this.makes = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(inventoryModel: IInventoryModel) {
    this.editForm.patchValue({
      id: inventoryModel.id,
      name: inventoryModel.name,
      vehicleType: inventoryModel.vehicleType,
      desc: inventoryModel.desc,
      makeId: inventoryModel.makeId
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const inventoryModel = this.createFromForm();
    if (inventoryModel.id !== undefined) {
      this.subscribeToSaveResponse(this.inventoryModelService.update(inventoryModel));
    } else {
      this.subscribeToSaveResponse(this.inventoryModelService.create(inventoryModel));
    }
  }

  private createFromForm(): IInventoryModel {
    const entity = {
      ...new InventoryModel(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      vehicleType: this.editForm.get(['vehicleType']).value,
      desc: this.editForm.get(['desc']).value,
      makeId: this.editForm.get(['makeId']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IInventoryModel>>) {
    result.subscribe((res: HttpResponse<IInventoryModel>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackMakeById(index: number, item: IMake) {
    return item.id;
  }
}
