import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IInventoryModel } from 'app/shared/model/inventory-model.model';
import { InventoryModelService } from './inventory-model.service';

@Component({
  selector: 'jhi-inventory-model-delete-dialog',
  templateUrl: './inventory-model-delete-dialog.component.html'
})
export class InventoryModelDeleteDialogComponent {
  inventoryModel: IInventoryModel;

  constructor(
    protected inventoryModelService: InventoryModelService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.inventoryModelService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'inventoryModelListModification',
        content: 'Deleted an inventoryModel'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-inventory-model-delete-popup',
  template: ''
})
export class InventoryModelDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ inventoryModel }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(InventoryModelDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.inventoryModel = inventoryModel;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/inventory-model', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/inventory-model', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
