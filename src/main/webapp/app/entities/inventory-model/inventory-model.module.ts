import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { HipcarzSharedModule } from 'app/shared';
import {
  InventoryModelComponent,
  InventoryModelDetailComponent,
  InventoryModelUpdateComponent,
  InventoryModelDeletePopupComponent,
  InventoryModelDeleteDialogComponent,
  inventoryModelRoute,
  inventoryModelPopupRoute
} from './';

const ENTITY_STATES = [...inventoryModelRoute, ...inventoryModelPopupRoute];

@NgModule({
  imports: [HipcarzSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    InventoryModelComponent,
    InventoryModelDetailComponent,
    InventoryModelUpdateComponent,
    InventoryModelDeleteDialogComponent,
    InventoryModelDeletePopupComponent
  ],
  entryComponents: [
    InventoryModelComponent,
    InventoryModelUpdateComponent,
    InventoryModelDeleteDialogComponent,
    InventoryModelDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HipcarzInventoryModelModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
