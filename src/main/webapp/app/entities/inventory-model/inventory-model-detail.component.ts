import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IInventoryModel } from 'app/shared/model/inventory-model.model';

@Component({
  selector: 'jhi-inventory-model-detail',
  templateUrl: './inventory-model-detail.component.html'
})
export class InventoryModelDetailComponent implements OnInit {
  inventoryModel: IInventoryModel;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ inventoryModel }) => {
      this.inventoryModel = inventoryModel;
    });
  }

  previousState() {
    window.history.back();
  }
}
