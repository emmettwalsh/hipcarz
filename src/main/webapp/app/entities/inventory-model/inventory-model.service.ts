import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IInventoryModel } from 'app/shared/model/inventory-model.model';

type EntityResponseType = HttpResponse<IInventoryModel>;
type EntityArrayResponseType = HttpResponse<IInventoryModel[]>;

@Injectable({ providedIn: 'root' })
export class InventoryModelService {
  public resourceUrl = SERVER_API_URL + 'api/inventory-models';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/inventory-models';

  constructor(protected http: HttpClient) {}

  create(inventoryModel: IInventoryModel): Observable<EntityResponseType> {
    return this.http.post<IInventoryModel>(this.resourceUrl, inventoryModel, { observe: 'response' });
  }

  update(inventoryModel: IInventoryModel): Observable<EntityResponseType> {
    return this.http.put<IInventoryModel>(this.resourceUrl, inventoryModel, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IInventoryModel>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IInventoryModel[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IInventoryModel[]>(this.resourceSearchUrl, { params: options, observe: 'response' });
  }
}
