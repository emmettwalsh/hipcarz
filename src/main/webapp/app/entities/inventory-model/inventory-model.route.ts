import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { InventoryModel } from 'app/shared/model/inventory-model.model';
import { InventoryModelService } from './inventory-model.service';
import { InventoryModelComponent } from './inventory-model.component';
import { InventoryModelDetailComponent } from './inventory-model-detail.component';
import { InventoryModelUpdateComponent } from './inventory-model-update.component';
import { InventoryModelDeletePopupComponent } from './inventory-model-delete-dialog.component';
import { IInventoryModel } from 'app/shared/model/inventory-model.model';

@Injectable({ providedIn: 'root' })
export class InventoryModelResolve implements Resolve<IInventoryModel> {
  constructor(private service: InventoryModelService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IInventoryModel> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<InventoryModel>) => response.ok),
        map((inventoryModel: HttpResponse<InventoryModel>) => inventoryModel.body)
      );
    }
    return of(new InventoryModel());
  }
}

export const inventoryModelRoute: Routes = [
  {
    path: '',
    component: InventoryModelComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'hipcarzApp.inventoryModel.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: InventoryModelDetailComponent,
    resolve: {
      inventoryModel: InventoryModelResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hipcarzApp.inventoryModel.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: InventoryModelUpdateComponent,
    resolve: {
      inventoryModel: InventoryModelResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hipcarzApp.inventoryModel.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: InventoryModelUpdateComponent,
    resolve: {
      inventoryModel: InventoryModelResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hipcarzApp.inventoryModel.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const inventoryModelPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: InventoryModelDeletePopupComponent,
    resolve: {
      inventoryModel: InventoryModelResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'hipcarzApp.inventoryModel.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
