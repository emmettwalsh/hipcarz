export * from './inventory-model.service';
export * from './inventory-model-update.component';
export * from './inventory-model-delete-dialog.component';
export * from './inventory-model-detail.component';
export * from './inventory-model.component';
export * from './inventory-model.route';
