export const enum VehicleType {
  SUV = 'SUV',
  SEDAN = 'SEDAN',
  TRUCK = 'TRUCK',
  COUPE = 'COUPE',
  CONVERTABLE = 'CONVERTABLE',
  WAGON = 'WAGON',
  HATCHBACK = 'HATCHBACK',
  MINIVAN = 'MINIVAN'
}

export interface IInventoryModel {
  id?: number;
  name?: string;
  vehicleType?: VehicleType;
  desc?: string;
  makeName?: string;
  makeId?: number;
}

export class InventoryModel implements IInventoryModel {
  constructor(
    public id?: number,
    public name?: string,
    public vehicleType?: VehicleType,
    public desc?: string,
    public makeName?: string,
    public makeId?: number
  ) {}
}
