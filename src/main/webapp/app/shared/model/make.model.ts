export interface IMake {
  id?: number;
  name?: string;
  desc?: string;
}

export class Make implements IMake {
  constructor(public id?: number, public name?: string, public desc?: string) {}
}
