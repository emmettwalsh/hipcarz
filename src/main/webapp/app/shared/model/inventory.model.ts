export interface IInventory {
  id?: number;
  vin?: string;
  year?: number;
  color?: string;
  comment?: string;
  inventoryModelName?: string;
  inventoryModelId?: number;
}

export class Inventory implements IInventory {
  constructor(
    public id?: number,
    public vin?: string,
    public year?: number,
    public color?: string,
    public comment?: string,
    public inventoryModelName?: string,
    public inventoryModelId?: number
  ) {}
}
