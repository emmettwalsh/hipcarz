import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HipcarzSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [HipcarzSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [HipcarzSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HipcarzSharedModule {
  static forRoot() {
    return {
      ngModule: HipcarzSharedModule
    };
  }
}
